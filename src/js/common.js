import './lib/assignPolyfill';
import 'svgxuse';
import objectFitImages from 'object-fit-images';
import PerfectScrollbar from 'perfect-scrollbar';
import { throttle, debounce } from 'throttle-debounce';
import  './components/_easing';
const { detect } = require('detect-browser');
const browser = detect();
(function($) {
  const WIN = $(window);
  const DOC = $(document);
  const BODY = $('body');
  const HTMLBODY = $('html, body');


  const HEADER = $('.header');
  const HEADERSTICKY = $('.header-sticky');
  const MAIN = $('.js-main');
  const FOOTER = $('.footer');

  let windowWidth = WIN.width();

  const BREAKPOINTS = {
    desktopLG: window.matchMedia('(max-width: 1499px)'),
    desktopMD: window.matchMedia('(max-width: 1279px)'),
    desktopSM: window.matchMedia('(max-width: 1023px)'),
    tabletLG: window.matchMedia('(max-width: 991px)')
  };



  const customThrottle = function(callback) {
    let active = false; // a simple flag
    let evt; // to keep track of the last event
    let handler = function() { // fired only when screen has refreshed
      active = false; // release our flag
      callback(evt);
    };
    return function handleEvent(e) { // the actual event handler
      evt = e; // save our event at each call
      if (!active) { // only if we weren't already doing it
        active = true; // raise the flag
        requestAnimationFrame(handler); // wait for next screen refresh
      };
    };
  };

  if(browser.name === 'firefox') {
    BODY.addClass('firefox');
  }else if (browser.name === 'ie') {
    BODY.addClass('ie');
  }else if (browser.name === 'edge') {
    BODY.addClass('edge');
  }else if (browser.name === 'chrome') {
    BODY.addClass('chrome');
  }



  const PREVENTDEFAULT = e => {
    e.preventDefault();
  };

  class Global {
    constructor() {
      this.isScrolling = false;
    }
    disableScroll() {
      WIN[0].addEventListener('touchmove', PREVENTDEFAULT, { passive: false });
    }
    enableScroll() {
      WIN[0].removeEventListener('touchmove', PREVENTDEFAULT, { passive: false });
    }
  }
  let G = new Global;


  class Header {
    constructor() {
      this.burger = $('.js-burger');
      this.isLand = Math.abs(window.orientation) === 90;

      this._init();
    }

    _init() {
      this._addOpenListener();
      // this._addScrollListener();

      this._cloneNavigation();

      this._onResize(BREAKPOINTS.tabletLG);
      BREAKPOINTS.tabletLG.addListener(this._onResize.bind(this));

      this._onOrientationChange();
    }

    _cloneNavigation() {
      const navigation = HEADER.find('.header__nav').clone();
      navigation.removeClass('header__nav col-dl-10 col-dl-push-1').addClass('wrapper').wrap('<div class="service-nav js-service-nav"></div>');
      this.navigation = navigation.parent();
      this.navigationInner = this.navigation.find('nav');
      this.navigationInner.wrap('<div class="row"><div class="col-tm-16 col-tm-push-11 col-mm-push-8"></div></div>');
      BODY.append(this.navigation);
    }

    _addOpenListener() {
      this.burger.on('click', e => {
        e.preventDefault();

        this.translateX = this._getTranslate();
        this.burger.toggleClass('is-active');
        this.navigation.toggleClass('is-active');
        this.navigationInner.css('padding-top', $(e.currentTarget).closest('header').height() + 30);
        BODY.toggleClass('is-fixed');

        this._slideContent();
      });

      BODY.on('click touchstart', e => {
        if (BODY.hasClass('is-fixed') &&
                    $(e.target).closest('.js-service-nav nav').length === 0 &&
                    !$(e.target).hasClass('.js-service-nav nav') &&
                    $(e.target).closest(this.burger).length === 0 &&
                    !$(e.target).hasClass(this.burger)) {
          this.burger.removeClass('is-active');
          this.navigation.removeClass('is-active');
          BODY.removeClass('is-fixed');
          G.enableScroll();
          // $('input').removeAttr('disabled');
          MAIN.css({
            'transform': 'translateX(0)'
          });

          FOOTER.css({
            'transform': 'translateX(0)'
          });
        }
      });
    }

    _getTranslate() {
      return this.navigationInner.outerWidth() + (this.navigation.width() - this.navigation.children().first().width())/2;
    }

    _slideContent() {
      if (BODY.hasClass('is-fixed')) {
        if (!this.isLand) {
          G.disableScroll();
        } else {
          G.enableScroll();
        }
        // $('input').attr('disabled', 'disabled');

        MAIN.css(
          'transform', `translateX(${-this.translateX}px)`
        );

        FOOTER.css(
          'transform', `translateX(${-this.translateX}px)`
        );

      } else {
        G.enableScroll();
        // $('input').removeAttr('disabled');

        MAIN.css({
          'transform': 'translateX(0)'
        });

        FOOTER.css({
          'transform': 'translateX(0)'
        });
      }
    }

    // _addScrollListener() {
    //   const hh = WIN.height()/3;
    //   DOC.on('scroll', throttle(250, () => {
    //     if (!G.isScrolling) {
    //       if (WIN.scrollTop() > hh) {
    //         if (!HEADERSTICKY.hasClass('is-sticky')) {
    //           HEADERSTICKY.addClass('is-sticky');
    //         }
    //       }
    //       else {
    //         if (HEADERSTICKY.hasClass('is-sticky')) {
    //           if (HEADERSTICKY.hasClass('is-sticky')) {
    //             HEADERSTICKY.removeClass('is-sticky');
    //           }
    //         }
    //       }
    //     }
    //   }));
    // }

    _onOrientationChange() {
      WIN.on('orientationchange', e => {
        this.isLand = Math.abs(window.orientation) === 90;
        this.translateX = this._getTranslate();
        this._slideContent();
      });
    }


    _onResize(point) {
      if (point.matches) {
        return;
      }
      else {
        this.burger.removeClass('is-active');
        this.navigation.removeClass('is-active');
        MAIN.removeAttr('style');
        FOOTER.removeAttr('style');
        BODY.removeClass('is-fixed');
        // G.enableScroll();
      }
    }
  }


  class StickyNav {
    constructor() {
      this.el = $('.js-sticky-nav');
      this.classes = {
        'scrollBlocks': '.js-sticky-scroll-block',
        'navLinks': '.js-sticky-nav-link',
        'navLists': '.js-sticky-nav-list'
      };
      this._start();
    }

    _start() {
      if(!this.el[0]) {
        DOC.on('scroll', throttle(250, () => {
          this._getHeaderState();
        }));
        return;
      }
      this.navList = $(this.classes.navLists);
      this._generateNavHtml();
    }


    _init() {
      this.isIE = BODY.hasClass('ie') ? true: false;
      this.isFF = BODY.hasClass('firefox') ? true: false;
      this.isChrome= BODY.hasClass('chrome') ? true: false;
      this.pseudo = $('.js-sticky-nav-pseudo');
      //  80 distance between nav and filter
      this.separateDistance = 80;
      this.topValue = +this.el.data('top');
      this._updateValues();
      MAIN.append(this.el);
      if(windowWidth >= 1024) {
        this.el.removeClass('is-disabled');
      }
      // this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.startTopPos}px, 0px)`});
      this.el.css({'transform' : `translate(${this.offsetLeft}px, ${this.startTopPos}px)`});

      this.links = $(this.classes.navLinks);
      // this.isFixed = false;
      // this.beforeFilter = false;
      this.projectFilter = $('.js-filter-box');
      this.isProjectsBehaviour = this.projectFilter[0] ? true: false;
      this.filterSpecOffset = this.separateDistance + this.topValue + this.pseudoHeight;
      this._getNavPosState_current;
      this._getNewPos_current;
      if(this.isProjectsBehaviour === true) {
        // this.projectFilter.css({'top': `${this.filterSpecOffset}px`});
        this.projectList = $('.js-projects-list-wrapper');
        // this.filterHeight = this.projectFilter.innerHeight();
        // this.projectListOffsetTop = this.projectList.offset().top;
        if (this.isIE === false) {
          // this._moreProjectsClick();
          this._getNewPos_current = this._getNewPos_Filter;
        }else{
          this._getNewPos_current = this._getNewPosIE_Filter;
        }

        this._getNavPosState_current = this._getNavPosState_Filter;
      }else{
        this._getNavPosState_current = this._getNavPosState;
      }
      this._getNavPosState_current();
      this._scroll();
      this._resize();
      this._onClick();
    }

    _generateNavHtml() {
      this.dataArr = [];
      $(this.classes.scrollBlocks).each((index, el) => {
        let $this = $(el);
        let obj = {'id': $this.data('id'), 'name': $this.data('name')};
        this.dataArr.push(obj);
      });
      this.navList.each((index, el) => {
        for(let i = 0; i < this.dataArr.length; i++) {
          let linkHtml = `<a href="${this.dataArr[i].id}" class="sticky-nav__item js-sticky-nav-link">${this.dataArr[i].name}</a>`;
          $(el).append(linkHtml);
        }
      });
      this._init();
    }


    _getNavPosState() {
      if(windowWidth < 1024) return;
      let winScrollTop = WIN.scrollTop();
      if(winScrollTop >= this.offsetTop) {
        // this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.topValue}px, 0px)`, 'position': 'fixed'});
        this.el.css({'transform' : `translate(${this.offsetLeft}px, ${this.topValue}px)`, 'position': 'fixed'});
        this._getActiveOnScroll(winScrollTop);
      }else{
        // this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.startTopPos}px, 0px)`, 'position': 'absolute'});
        this.el.css({'transform' : `translate(${this.offsetLeft}px, ${this.startTopPos}px)`, 'position': 'absolute'});
      }
    }

    _getNavPosState_Filter() {
      if(windowWidth < 1024) return;
      let winScrollTop = WIN.scrollTop();
      if(winScrollTop >= this.offsetTop) {
        let projectListOffsetTop = this.projectList.offset().top;
        let filterOffset = projectListOffsetTop - winScrollTop - this.filterSpecOffset;

        if(filterOffset <= 0 ) {
          let newPos = this._getNewPos_current(filterOffset, winScrollTop);
          // this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${newPos}px, 0px)`, 'position': 'absolute'});
          this.el.css({'transform' : `translate(${this.offsetLeft}px, ${newPos}px)`, 'position': 'absolute'});
        }else{
          // this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.topValue}px, 0px)`, 'position': 'fixed'});
          this.el.css({'transform' : `translate(${this.offsetLeft}px, ${this.topValue}px)`, 'position': 'fixed'});
        }
        this._getActiveOnScroll(winScrollTop);
        //
      }else{
        // this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.startTopPos}px, 0px)`, 'position': 'absolute'});
        this.el.css({'transform' : `translate(${this.offsetLeft}px, ${this.startTopPos}px)`, 'position': 'absolute'});
      }
    }

    _resize() {
      let self = this;
      this.resizeFunc = throttle(300, () => {
        self._updateValues(true);
      });
      WIN.on('resize', this.resizeFunc);

      // if(windowWidth >= 1024 ) {
      //   this.resizeFunc = throttle(300, () => {
      //     self._updateValues(true);
      //   });
      //   WIN.on('resize', this.resizeFunc);
      // }else{
      //   let debounceFunc = debounce(500, () => {
      //
      //     self._destroy();
      //   });
      //   WIN.on('orientationchange', debounceFunc);
      // }
    }
    _destroy() {
      this.offsetTop = 0;
      this.offsetLeft = 0;
      this.pseudoHeight = 0;
      this.headersHeight = 0;
      this.filterSpecOffset = 0;
      this.startTopPos = 0;
      this.dataArr.length = 0;
      this.el.removeAttr('style');
      this.navList.each((index, el) => {
        for(let i = 0; i < dataArr.length; i++) {
          $(el).html('');
        }
      });
      window.removeEventListener('scroll', customThrottle(this._scrollFunc.bind(this)));
    }

    _updateValues(callNavStateFunc) {

      windowWidth = WIN.width();
      if(windowWidth < 1024) {
        this.el.addClass('is-disabled');
        return;
      }else{
        this.el.removeClass('is-disabled');
      };

      this.offsetTop = this.pseudo.offset().top - this.topValue;
      this.offsetLeft = this.pseudo.offset().left;
      this.pseudoHeight = this.pseudo.innerHeight();
      this.headersHeight = $('.js-headers').innerHeight();
      this.filterSpecOffset = this.separateDistance + this.topValue + this.pseudoHeight;
      // this.startTopPos = this.pseudo.offset().top - this.headersHeight;
      if(this.isIE === true) {
        this.startTopPos = this.pseudo.offset().top;
      }else{
        this.startTopPos = this.pseudo.offset().top - this.headersHeight;
      }
      if(callNavStateFunc === true) {
        this._getNavPosState_current();
      }
    }

    _scrollFunc(e) {
      this._getNavPosState_current();
      this._getHeaderState();

    }

    _scroll() {
      // let self = this;
      if(this.isChrome === true) {
        // console.log('chrome scrolling');
        window.addEventListener('scroll', customThrottle(this._scrollFunc.bind(this)));
        // this.throttleFunc = throttle(10, this._scrollFunc.bind(this));
        // WIN.on('scroll', this.throttleFunc);
      }else{
        // console.log('else');
        this.throttleFunc = throttle(10, this._scrollFunc.bind(this));
        WIN.on('scroll', this.throttleFunc);

        // window.addEventListener('scroll', customThrottle(this._scrollFunc.bind(this)));
      }
    }

    _getActiveOnScroll(scrollTop) {
      // let scrollPos = WIN.scrollTop();
      $(this.classes.scrollBlocks).each((i, el) => {
        let $this  = $(el);
        let headerHeight = $('.js-headers').find('.header-sticky').innerHeight();
        if ($this.offset().top - headerHeight <= scrollTop && $this.offset().top + $this.innerHeight() > scrollTop + headerHeight) {
          this.el.find('a').removeClass('is-active');
          let data = $this.data('id');
          this.el.find(`${this.classes.navLinks}[href="${data}"]`).addClass('is-active');
          return false;
        }else{
          this.el.find('a').removeClass('is-active');
        }
      });
    }

    _onClick() {
      // let self = this;
      this.links.on('click', (e) => {
        e.preventDefault();
        let $this = $(e.target);
        // if($this.hasClass('is-active')) return;
        let target =  $(`${this.classes.scrollBlocks}[data-id="${$this.attr('href')}"]`);
        this.el.find('a').removeClass('is-active');
        let headerHeight = $('.js-headers').find('.header-sticky').innerHeight();
        $('html, body').stop().animate({
          'scrollTop': target.offset().top - headerHeight + 1
        }, 1200, 'easeInOutCubic', function() {
          // WIN.on('scroll', self.throttleFunc);
        });
      });
    }

    _getNewPos_Filter(filterOffset, winScrollTop) {
      return Math.round(filterOffset + winScrollTop + this.topValue - this.headersHeight);
    }
    _getNewPosIE_Filter(filterOffset, winScrollTop) {
      return Math.round(filterOffset + winScrollTop + this.topValue);
    }
    _moreProjectsClick() {
      $('.js-more-btn').on('click', (e) => {
        e.preventDefault();
        let interval = setInterval(() => {
          if(window.projectsMoreClicked === true) {
            window.projectsMoreClicked = false;
            clearInterval(interval);
            this._getNavPosState_current();
            console.log('more click from topBanner.js');
          }
        },50);
      });
    }

    _getHeaderState() {
      let hh = WIN.height()/3;
      if (!G.isScrolling) {
        if (WIN.scrollTop() > hh) {
          if (!HEADERSTICKY.hasClass('is-sticky')) {
            HEADERSTICKY.addClass('is-sticky');
          }
        }
        else {
          if (HEADERSTICKY.hasClass('is-sticky')) {
            if (HEADERSTICKY.hasClass('is-sticky')) {
              HEADERSTICKY.removeClass('is-sticky');
            }
          }
        }
      }
    }
  }

  // class Main {
  //   constructor() {
  //     this.main = $('.main');

  //     this._init();
  //   }

  //   _init() {
  //     this._count();
  //     WIN.on('resize', throttle(200, () => {
  //       this._count();
  //     }));
  //   }

  //   _count() {
  //     this.main.css('paddingTop', HEADER.height());
  //   }
  // }
  // DOC.ready(() => {
  //   if (!document.querySelector('.gallery')) {
  //     new Main();
  //   }
  // });

  class Modal {
    constructor() {
      this.modal = $('.js-modal');
      this.button = $('[data-modal-id]');
      this.close = this.modal.find('.js-modal-close');
      this.scrollTop = 0;
      this.activeScrollBlock;
      this.contentWrapper = this.modal.find('.js-modal-content-wrapper');
      this.wpRequest = this.modal.data('wp-request');

      // this.scrollBlocks = this.modal.find('.js-ps-scroll');
      // this.scrollInits = [];
    }

    init() {
      this.modal.on('click', this._onModalClick.bind(this));
      this.button.on('click', this._open.bind(this));
      this.close.on('click', this._close.bind(this));

      this._resize();
      // this._updateModalScroll();
      // this.scrollBlocks.each((index, el) => {
      //   let ps = new PerfectScrollbar(el);
      // });

    }

    _open(e) {
      e.preventDefault();
      // console.log('modal open');
      let $el = $(e.currentTarget);
      let id = $el.data('modal-id');
      let quoteId = $el.data('quote-id') === undefined ? '': $el.data('quote-id');

      this.scrollTop = WIN.scrollTop();
      // console.log(this.scrollTop);
      // this.targetModal = $(e.currentTarget).data('target-modal');
      // let targetEl = $(`[data-modal="${this.targetModal}"]`);
      // this.modal.addClass('is-open');
      //       // BODY.addClass('is-modal-open');
      //       //
      //       // this._updateModalScroll(this.modal);
      this._request(id, quoteId);

    }
    _request(id, quoteId) {

      if(this.wpRequest === true) {

        let self = this;
        let data = {
          action: 'expertjson', // Do not change this line. Wordpress Magic.
          expert_id: id, // Here ID which you should get from data attr.
          quote_id: quoteId
        };
        jQuery.post(myajax.url, data, function(response) { // Do not change this line. WordPress Magic.
          let data = JSON.parse(response); // JSON of result

          //generatePopup
          self._generateHtml(data);
        });


      }else{

        // console.log('is modal.json');
        $.ajax({
          url:  'modal.json',
          dataType: 'json',
          success: (response) => {
            let data = response[0];
            this._generateHtml(data);
          }
        });
      }
    }
    _generateHtml(data) {
      let html = `
              <div class="expert__image">
                 <img src="${data.image}" alt=""/>
              </div>
               <div class="expert__in">
                   <div class="expert__title">
                       <h2>${data.name}</h2>
                       <h4>${data.hobby}, ${data.working_evergy_since}</h4>
                   </div>
                   <div class="expert__desc">
                       <p><strong>native language: </strong>${data.language}</p>
                       <p><strong>working in the sector since: </strong>${data.working_in_the_sector_since}</p>
                   </div>
                   <div class="expert__before-quote-text">${data.education}</div>
                   <blockquote class="expert__quote">${data.quote}</blockquote>
                   <div class="expert__text">${data.quote_translated}</div>
               </div>
        `;
      this.contentWrapper.html(html);
      this.modal.addClass('is-open');
      BODY.addClass('is-modal-open');

      this._updateModalScroll(this.modal);
    }
    _resize() {

      $(window).on('resize ', debounce(400, () => {
        let $el = $('.js-modal').find('.js-ps-scroll');
        $el.css('height', '0px');
        let parentHeight = $el.parent().height();
        $el.css('height', `${parentHeight}px`);
        // console.log(parentHeight);
        if(this.activeScrollBlock !== undefined) {
          this.activeScrollBlock.update();

        }

        // $('.js-map-popup-body').css('height', `${contentH}px`);
        // if (this.ps) {
        //   this.ps.update();
        // }
      }));
    }
    _updateModalScroll(modal) {

      let scrollBlock = modal.find('.js-ps-scroll');
      let scrollBlockHeight = scrollBlock.parent().height();
      scrollBlock.css('height', `${scrollBlockHeight}px`);

      this.activeScrollBlock = new PerfectScrollbar(scrollBlock[0],{
        suppressScrollX: true
      });

      // });
      // $('.js-ps-scroll').each((index, el) => {
      //   console.log(111);
      //   let ps = new PerfectScrollbar(el);
      // });
    }

    _close(e) {
      e.preventDefault();
      // this.openedModal = $(e.currentTarget).closest('[data-modal]');
      // this.openedModal.removeClass('is-open');
      this.modal.removeClass('is-open');
      BODY.removeClass('is-modal-open');
      HTMLBODY.animate({
        scrollTop: this.scrollTop
      }, 0);

      this.activeScrollBlock.destroy();
      this.activeScrollBlock = undefined;
      this.contentWrapper.html('');

    }

    _onModalClick(e) {
      if ($(e.target).closest('.js-modal').length !== 0 || $(e.target).hasClass('.js-modal')) return;
      this.modal.removeClass('is-open');
      BODY.removeClass('is-modal-open');
    }

  }

  class ScrollOtherPage {
    constructor() {
      this.el = $('.js-scroll-other-page-link');
      this._init();
      this._click();

    }
    _init() {
      if(!window.sessionStorage) return;

      let storageValue = window.sessionStorage.getItem('scrollOtherPage');
      if(storageValue !== null) {
        this._scrollTo(storageValue);
      }
    }
    _scrollTo(target) {
      window.sessionStorage.setItem('scrollOtherPage', null);
      // console.log(`session storage: ${target}`);
      let $target = $(`[data-other-page-scroll-id="${target}"]`);
      if(!$target[0]) return;

      this._scrollFunc($target);
      // let projects = $('.js-projects');
      // if(projects[0]) {
      //
      //   let interval = setInterval(() => {
      //     if(window.projectsLoaded === true) {
      //
      //       clearInterval(interval);
      //       this._scrollFunc($target);
      //     }
      //   },100);
      // }else{
      //
      //   this._scrollFunc($target);
      // }


    }
    _scrollFunc(target, delay = 300) {
      let headerH = $('.js-headers .header-sticky').innerHeight();
      setTimeout(() => {
        HTMLBODY.animate({
          scrollTop: target.offset().top +1 - headerH
        }, 1200, 'easeInOutCubic',() => {
          // console.log(`11 ${window.sessionStorage.getItem('scrollOtherPage')}`);
        });
      }, delay);
    }
    _click() {
      this.el.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        let href = $(this).attr('href') !== undefined ? $(this).attr('href') : $(this).attr('data-href');
        let hashPos =  href.indexOf('#');
        let url = href.substring(0, hashPos);
        let target = href.substring(hashPos+1);
        window.sessionStorage.setItem('scrollOtherPage', target);

        document.location.href = url;

      });
    }
  }
  function isTouch() {
    return navigator.userAgent.match(/iPad|iPhone|Android/i) || (navigator.userAgent.includes('Mac') && 'ontouchend' in document);
  }
  DOC.ready(() => {
    if(isTouch()) BODY.addClass('is-touch');
    objectFitImages();
    new Header();
    new StickyNav();
    new Modal().init();
    // new StickyNav();
    new ScrollOtherPage();

    $('.js-clear-storage').on('click', function(e) {
      e.preventDefault();
      let href = $(this).attr('href');
      clearTimelineStorage();
      clearProjectsStorage();
      clearMapStorageData();
      window.open(href, '_self' );
    });

    function clearTimelineStorage() {
      // let storageValue = window.sessionStorage.getItem('timelineActiveIndex');
      // if(storageValue !== null && storageValue !== undefined) {
      window.sessionStorage.setItem('timelineActiveIndex', 'null');
      // }
    }
    function clearProjectsStorage() {
      window.sessionStorage.setItem('projectsHtmlString', 'null');
      window.sessionStorage.setItem('projectsPostPage', 'null');
      window.sessionStorage.setItem('projectsActiveRequestType', 'null');
      window.sessionStorage.setItem('projectsIsNoMoreBlocks', 'null');
      window.sessionStorage.setItem('projectsPageId', 'null');
    }
  });

  function clearMapStorageData() {
    window.sessionStorage.setItem('myMapActiveProjectId', 'null');
    window.sessionStorage.setItem('myMapZoom', 'null');
    window.sessionStorage.setItem('myMapCoordLng', 'null');
    window.sessionStorage.setItem('myMapcoordLat', 'null');
  }

  // remove this in release
  DOC.on('keydown', e => {
    if (e.keyCode === 71 && e.ctrlKey) {
      $('.grid').toggleClass('is-visible');
    }
  });
})(jQuery);
