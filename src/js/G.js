import { BODY, DOC, WIN, PREVENTDEFAULT } from './global';

class Global {
  constructor() {
    this.isScrolling = false;
  }


  disableScroll() {
    WIN[0].addEventListener('touchmove', PREVENTDEFAULT, { passive: false });
  }

  enableScroll() {
    WIN[0].removeEventListener('touchmove', PREVENTDEFAULT, { passive: false });
  }
}

export let G = new Global;
