import Swiper from 'swiper/dist/js/swiper';

(function($) {
  const WIN = $(window);
  const DOC = $(document);

  class QuoteSlider {
    constructor() {
      this.slider = $('.js-quote-slider');
      this.slidesCount = this.slider.find('.swiper-wrapper > div').length;
      this.pagination = this.slider.find('.swiper-pagination');
      this.arrowPrev = this.slider.find('.js-quote-prev');
      this.arrowNext = this.slider.find('.js-quote-next');
        
      this._init();
    }

    _init() {
      if(!this.slider[0]) return;
      if(this.slidesCount > 1) {
        this.slider.parent().addClass('pb');
        this.swiper = new Swiper(this.slider, this._getOptions());
      }else{
        this.arrowPrev.addClass('hidden');
        this.arrowNext.addClass('hidden');
        this.pagination.addClass('hidden');
      }

    }

    _getOptions() {
      return {
        speed: 1000,
        loop: true,
        simulateTouch: false,
        pagination: {
          el: this.pagination,
          clickable: true
        },
        navigation: {
          nextEl: this.arrowNext,
          prevEl: this.arrowPrev
        }
      };
    }
  }

  DOC.ready(() => {
    new QuoteSlider();
  });
})(jQuery);
