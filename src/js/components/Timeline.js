// import Parallax from 'parallax-js';
// import '@babel/polyfill';
// import Parlx from 'parlx.js';
import { TweenLite } from 'gsap/TweenMax';
import BezierEasing from 'bezier-easing';

(function($) {
  const WIN = $(window);
  const DOC = $(document);
  const HTMLBODY = $('html, body');

  class Timeline {
    constructor() {
      this.accordion = $('.js-timeline');
      this.accordionTitle = $('.js-timeline-title');
      this.accordionContent = $('.js-timeline-content');
      this.storageBtn = $('.js-timeline-storage-btn');
      this.isAccordionMoving = false;
      this.isFirstClick = false;
      this.isFirstOpen = false;
      this.easing = BezierEasing(0.64, 0.04, 0.35, 1);

      this._init();
    }

    _init() {
      this.accordionTitle.on('click', this._onClick.bind(this));
      this.storageBtn.on('click', this._storageBtnClick.bind(this));
      this._findElemInStorage();

    }
    _storageBtnClick(e) {
      e.preventDefault();
      const $el = $(e.currentTarget);
      const activeElem = $el.parents('.js-timeline');
      const activeIndex = this.accordion.index(activeElem);
      const href = $el.attr('href');
      window.sessionStorage.setItem('timelineActiveIndex', activeIndex);
      window.open(href, '_self' );

    }
    _findElemInStorage() {
      let storageValue = window.sessionStorage.getItem('timelineActiveIndex');
      // null by default, but on sessionStorage.setItem null === 'null'
      if(storageValue !== 'null' && storageValue !== null && this.accordion[0]) {
        window.sessionStorage.setItem('timelineActiveIndex', null);
        const index = parseInt(storageValue);
        const accordion = $('.js-timeline').eq(index);
        const accordionContent = accordion.find('.js-timeline-content');
        this._open(accordion, accordionContent, 0);
        this.isFirstOpen = true;
      }

    }
    _open(accordion, accordionContent, time = 0.6) {
      accordion.addClass('is-active');
      this.currentContent = accordionContent;
      this.isAccordionMoving = true;
      TweenLite.set(accordionContent, {height: 'auto', visibility: 'visible'});
      TweenLite.from(accordionContent, time, {height: 0, ease: this.easing, onComplete: () => {
        // this.currentContent = accordionContent;
        this.isAccordionMoving = false;
        this._getSizesForScroll();
        this._scrollToStart();
      }});
    }
    _scrollToStart(scrollSpeed = 800) {
      HTMLBODY.animate({
        scrollTop: this.accordion.filter('.is-active').offset().top - this.scrollOffset
      }, scrollSpeed, 'easeInOutCubic', () => {
      });
    }
    _getSizesForScroll() {
      let additionalOffset = 10;
      this.scrollOffset = $('.js-headers .header-sticky').innerHeight() + additionalOffset;
    }

    _close() {
      this.accordion.removeClass('is-active ');
      this.isAccordionMoving = true;
      const currentContent = this.currentContent;
      TweenLite.to(currentContent, 0.6, {height: 0, ease: this.easing, onComplete: () => {
        this.isAccordionMoving = false;
        TweenLite.set(currentContent, {visibility: 'hidden'});
      }});
    }

    _onClick(e) {
      e.preventDefault();
      if (this.isAccordionMoving) return;
      this.isFirstClick = true;
      const accordion = $(e.currentTarget).closest('.js-timeline');
      const accordionContent = accordion.find('.js-timeline-content');
      const index = accordion.index()-1;
      if (!accordion.hasClass('is-active')) {
        if (!this.isFirstOpen) {
          this.isFirstOpen = true;
        } else {
          this._close();
        }

        this._open(accordion, accordionContent);
      }
      else {
        this._close();
        this.isFirstOpen = false;
      }
    }
  }

  DOC.ready(() => {
    new Timeline();
  });
})(jQuery);
