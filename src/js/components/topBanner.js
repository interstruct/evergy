import { throttle, debounce } from 'throttle-debounce';

import  './_easing';


(function($) {

  // return;
  console.log('helo from topBanner.js');
  const WIN = $(window);
  const DOC = $(document);
  const BODY = $('body');
  let windowWidth = WIN.width();
  const customThrottle = function(callback) {
    let active = false; // a simple flag
    let evt; // to keep track of the last event
    let handler = function() { // fired only when screen has refreshed
      active = false; // release our flag
      callback(evt);
    };
    return function handleEvent(e) { // the actual event handler
      evt = e; // save our event at each call
      if (!active) { // only if we weren't already doing it
        active = true; // raise the flag
        requestAnimationFrame(handler); // wait for next screen refresh
      };
    };
  };
  class StickyNav {
    constructor() {
      this.el = $('.js-sticky-nav');
      this.classes = {
        'scrollBlocks': '.js-sticky-scroll-block',
        'navLinks': '.js-sticky-nav-link',
        'navLists': '.js-sticky-nav-list'
      };
      this._start();
    }

    _start() {
      if(!this.el[0]) return;
      this.navList = $(this.classes.navLists);
      this._generateNavHtml();
    }

    _init() {
      this.isIE = BODY.hasClass('ie') ? true: false;
      this.isFF = BODY.hasClass('firefox') ? true: false;
      this.isChrome= BODY.hasClass('chrome') ? true: false;
      this.pseudo = $('.js-sticky-nav-pseudo');
      //  80 distance between nav and filter
      this.separateDistance = 80;
      this.topValue = +this.el.data('top');
      this._updateValues();
      $('.js-main').append(this.el);

      this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.startTopPos}px, 0px)`});
      if(WIN.width() >= 1024) {
        this.el.removeClass('is-disabled');
      }

      this.links = $(this.classes.navLinks);
      this.isFixed = false;
      this.behindFilter = false;
      this.projectFilter = $('.js-filter-box');
      this.isProjectsBehaviour = this.projectFilter[0] ? true: false;
      this.filterSpecOffset = this.separateDistance + this.topValue + this.pseudoHeight;
      this._getNavPosState_current;
      this._getNewPos_current;
      if(this.isProjectsBehaviour === true) {
        this.projectFilter.css({'top': `${this.filterSpecOffset}px`});
        this.projectList = $('.js-projects-list-wrapper');
        // this.filterHeight = this.projectFilter.innerHeight();
        // this.projectListOffsetTop = this.projectList.offset().top;
        if (this.isIE === false) {
          this._moreProjectsClick();
          this._getNewPos_current = this._getNewPos_Filter;
        }else{
          this._getNewPos_current = this._getNewPosIE_Filter;
        }
        this._getNavPosState_current = this._getNavPosState_Filter;
      }else{
        this._getNavPosState_current = this._getNavPosState;
      }
      this._getNavPosState_current();
      this._scroll();
      this._resize();
      this._onClick();
    }

    _generateNavHtml() {
      let dataArr = [];
      $(this.classes.scrollBlocks).each((index, el) => {
        let $this = $(el);
        let obj = {'id': $this.data('id'), 'name': $this.data('name')};
        dataArr.push(obj);
      });
      this.navList.each((index, el) => {
        for(let i = 0; i < dataArr.length; i++) {
          let linkHtml = `<a href="${dataArr[i].id}" class="sticky-nav__item js-sticky-nav-link">${dataArr[i].name}</a>`;
          $(el).append(linkHtml);
        }
      });
      this._init();
    }

    _getNavPosState() {
      // console.log('simple');
      let winScrollTop = WIN.scrollTop();
      if(winScrollTop >= this.offsetTop) {
        if(this.isFixed === false) {
          this.isFixed = true;
          this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.topValue}px, 0px)`, 'position': 'fixed'});
        }
        this._getActiveOnScroll(winScrollTop);
      }else{
        if(this.isFixed === true) {
          this.isFixed = false;
          this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.startTopPos}px, 0px)`, 'position': 'absolute'});
        }else{
          this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.startTopPos}px, 0px)`});
        }
      }
    }

    _getNavPosState_Filter() {
      // console.log('filter');
      let winScrollTop = WIN.scrollTop();
      if(winScrollTop >= this.offsetTop) {
        if(this.isFixed === false) {
          // this.el.addClass('is-visible');
          this.isFixed = true;
          this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.topValue}px, 0px)`, 'position': 'fixed'});
        }
        let projectListOffsetTop = this.projectList.offset().top;
        // let filterHeight = this.projectFilter.innerHeight();
        let filterOffset = projectListOffsetTop - winScrollTop - this.filterSpecOffset;
        if(filterOffset <= 0 ) {
          if(this.behindFilter === false) {
            let newPos = this._getNewPos_current(filterOffset, winScrollTop);
            this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${newPos}px, 0px)`, 'position': 'absolute'});
            this.behindFilter = true;
          }
        }else{
          if(this.behindFilter === true) {
            this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.topValue}px, 0px)`, 'position': 'fixed'});
            this.behindFilter = false;
          }
        }
        this._getActiveOnScroll(winScrollTop);
        //
      }else{
        if(this.isFixed === true) {
          this.isFixed = false;
          this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.startTopPos}px, 0px)`, 'position': 'absolute'});
        }else{
          this.el.css({'transform' : `translate3d(${this.offsetLeft}px, ${this.startTopPos}px, 0px)`});
        }
      }
    }

    _scroll() {
      // let self = this;
      if(this.isChrome === true) {
        console.log('chrome scrolling');
        // window.addEventListener('scroll', customThrottle(this._scrollFunc.bind(this)));
        this.throttleFunc = throttle(10, this._scrollFunc.bind(this));
        WIN.on('scroll', this.throttleFunc);
      }else{
        this.throttleFunc = throttle(10, this._scrollFunc.bind(this));
        WIN.on('scroll', this.throttleFunc);
        // WIN.on('scroll', this._scrollFunc.bind(this));
        // window.addEventListener('scroll', customThrottle(this._scrollFunc.bind(this)));
      }
    }
    _getNewPos_Filter(filterOffset, winScrollTop) {
      return Math.round(filterOffset + winScrollTop + this.topValue - this.headersHeight);
    }
    _getNewPosIE_Filter(filterOffset, winScrollTop) {
      return Math.round(filterOffset + winScrollTop + this.topValue);
    }
    _scrollFunc(e) {
      if(windowWidth < 1024) return;
      this._getNavPosState_current();

    }
    _getActiveOnScroll(scrollTop) {
      // let scrollPos = WIN.scrollTop();
      $(this.classes.scrollBlocks).each((i, el) => {
        let $this  = $(el);
        let headerHeight = $('.js-headers').find('.header-sticky').innerHeight();
        if ($this.offset().top - headerHeight <= scrollTop && $this.offset().top + $this.innerHeight() > scrollTop + headerHeight) {
          this.el.find('a').removeClass('is-active');
          let data = $this.data('id');
          this.el.find(`${this.classes.navLinks}[href="${data}"]`).addClass('is-active');
          return false;
        }else{
          this.el.find('a').removeClass('is-active');
        }
      });
    }

    _onClick() {
      // let self = this;
      this.links.on('click', (e) => {
        e.preventDefault();
        let $this = $(e.target);
        // if($this.hasClass('is-active')) return;
        let target =  $(`${this.classes.scrollBlocks}[data-id="${$this.attr('href')}"]`);
        this.el.find('a').removeClass('is-active');
        let headerHeight = $('.js-headers').find('.header-sticky').innerHeight();
        $('html, body').stop().animate({
          'scrollTop': target.offset().top - headerHeight + 1
        }, 1200, 'easeInOutCubic', function() {
          // WIN.on('scroll', self.throttleFunc);
        });
      });
    }

    _resize() {
      let self = this;
      this.resizeFunc = throttle(300, () => {
        // this.resizeFunc = debounce(350, () => {
        // if(WIN.width() >= 1024) {
        self._updateValues(true);
        // }
      });
      WIN.on('resize', this.resizeFunc);
    }

    _updateValues(callNavStateFunc) {
      windowWidth = WIN.width();
      if(windowWidth < 1024) {
        this.el.addClass('is-disabled');
        return;
      }else{
        this.el.removeClass('is-disabled');
      };

      this.offsetTop = this.pseudo.offset().top - this.topValue;
      this.offsetLeft = this.pseudo.offset().left;
      this.pseudoHeight = this.pseudo.innerHeight();
      this.headersHeight = $('.js-headers').innerHeight();
      this.filterSpecOffset = this.separateDistance + this.topValue + this.pseudoHeight;
      // this.startTopPos = this.pseudo.offset().top - this.headersHeight;
      if(this.isIE === true) {
        this.startTopPos = this.pseudo.offset().top;
      }else{
        this.startTopPos = this.pseudo.offset().top - this.headersHeight;
      }
      if(callNavStateFunc === true) {
        this._getNavPosState_current();
      }
    }
    _moreProjectsClick() {
      $('.js-more-btn').on('click', (e) => {
        e.preventDefault();
        let interval = setInterval(() => {
          if(window.projectsMoreClicked === true) {
            window.projectsMoreClicked = false;
            clearInterval(interval);
            this._getNavPosState_current();
            console.log('more click from topBanner.js');
          }
        },50);
      });
    }
  }
  DOC.ready(() => {

    DOC.StickyNav = new StickyNav();

  });
})(jQuery);
