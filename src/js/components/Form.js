(function($) {

  const DOC = $(document);

  class Form {
    constructor(form) {
      this.form = $(form);
      this.realInput = this.form.find('input:not([type="checkbox"], [type="radio"]), textarea:not(.g-recaptcha-response)');
      this.submitButton = this.form.find('.js-form-submit');
      this._init();
    }

    _init() {
      if(!this.form[0]) return;
      console.log('form');
      this._checkEmpty();
      this._listen();
      this.submitButton.on('click', this._listenSuccess.bind(this));
    }

    _checkEmpty() {
      this.realInput.each((index, input) => {
        $(input).on('focus', this._onFocus.bind(this));
        $(input).on('blur', this._onBlur.bind(this));
      });
    }

    _onFocus(e) {
      const t = $(e.currentTarget);
      t.closest('.input').addClass('is-active');
    }

    _onBlur(e) {
      const t = $(e.currentTarget);
      if (t.val() === '') t.closest('.input').removeClass('is-active');
    }

    _listenSuccess() {
      const message = $('.wpcf7-response-output');
      const messageObserver = new MutationObserver(callback);

      function callback(mutationList) {
        mutationList.forEach(function(mutation) {
          switch(mutation.type) {
            case 'attributes':
              switch(mutation.attributeName) {
                case 'class':
                  if ($(mutation.target).hasClass('wpcf7-mail-sent-ok')) {
                    $('.input').removeClass('is-active is-error');
                  }
                  break;
              }
              break;
          }
        });
      }

      const observer = new MutationObserver(callback);
      observer.observe(message[0], {
        attributeFilter: [ 'class' ],
        attributeOldValue: true,
        subtree: true
      });
    }

    _listen() {
      const fields = this.form.find('.wpcf7-form-control-wrap');

      const fieldObserver = new MutationObserver(mutations => {
        mutations.forEach(mutation => {
          if (mutation.addedNodes.length !== 0) {
            $(mutation.target).closest('.input').addClass('is-error');
          }
          if (mutation.addedNodes.length === 0) {
            $(mutation.target).closest('.input').removeClass('is-error');
          }
        });
      });

      fields.each((index, field) => {
        fieldObserver.observe(field, {
          childList: true
        });
      });
    }
  }

  DOC.ready(() => {
    new Form('.js-hello-form');
  });

})(jQuery);
