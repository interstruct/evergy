import { throttle, debounce } from 'throttle-debounce';
import Stickyfill from 'stickyfilljs';

(function($) {
  const WIN = $(window);
  const DOC = $(document);
  const BODY = $('body');
  const HTMLBODY = $('html, body');

  const HEADER = $('.header');
  const HEADERSTICKY = $('.header-sticky');
  const MAIN = $('.main');
  const FOOTER = $('.footer');

  const BREAKPOINTS = {
    desktopLG: window.matchMedia('(max-width: 1499px)'),
    desktopMD: window.matchMedia('(max-width: 1279px)'),
    desktopSM: window.matchMedia('(max-width: 1023px)'),
    tabletLG: window.matchMedia('(max-width: 991px)')
  };

  const PREVENTDEFAULT = e => {
    e.preventDefault();
  };


  class Global {
    constructor() {
      this.isScrolling = false;
    }


    disableScroll() {
      WIN[0].addEventListener('touchmove', PREVENTDEFAULT, { passive: false });
    }

    enableScroll() {
      WIN[0].removeEventListener('touchmove', PREVENTDEFAULT, { passive: false });
    }
  }

  let G = new Global;


  class StickyNavigation {
    constructor() {
      this.pseudo = $('.js-st-pseudo-nav');
      this.filter = $('.js-filter-box');
      this.scrollBlocks = $('.js-sticky-scroll-block');

      this._init();
    }

    _init() {
      this._createNav();
      WIN.on('scroll', throttle(66, this._scroll.bind(this)));
      WIN.on('resize', throttle(300, this._resize.bind(this)));

      $('.js-st-link').on('click', this._click.bind(this));
    }

    _count() {
      console.log(this.pseudo.offset().top, this.pseudo.height());
      this.dimensions = {
        'top': Math.round(this.pseudo.offset().top),
        'margin-left': Math.round(this.pseudo.offset().left)
      };

      this.styles = {
        'position': 'absolute',
        'top': 0,
        'left': 0,
        'width': '100%',
        'height': Math.round(this.filter.offset().top - MAIN.offset().top - 80)
      };
    }

    _resize() {
      this._count();

      this.nav.css(this.dimensions);
      this.wrap.css(this.styles);
    }

    _createNav() {
      this._createList();
      this._count();

      const nav = this.pseudo.clone();
      nav
        .removeClass('js-st-pseudo-nav')
        .addClass('st-nav_sticky')
        .addClass('js-st-nav')
        .css(this.dimensions);

      const wrap = nav.wrap('<div class="st-nav-wrap js-st-nav-wrap"></div>').parent();
      wrap
        .css(this.styles);

      const list = nav.find('.js-st-pseudo-list');
      list
        .removeClass('js-st-pseudo-list')
        .addClass('js-st-list');

      MAIN.prepend(wrap);
      this.nav = $('.js-st-nav');
      this.wrap = $('.js-st-nav-wrap');

      Stickyfill.add(this.nav);
    }

    _createList() {
      const list = $('.js-st-pseudo-list');
      let liArr = [];
      this.scrollBlocks.each((index, el) => {
        liArr.push({
          'id': $(el).data('id'),
          'name': $(el).data('name')
        });
      });

      liArr.forEach(el => {
        let li = `<a href="${el.id}" class="st-nav__link js-st-link">${el.name}</a>`;
        list.append(li);
      });
    }

    _scroll() {
      let scrollTop = WIN.scrollTop();
      this.scrollBlocks.each((index, el) => {
        let data = $(el).data('id');
        let padding = window.getComputedStyle(el, null).getPropertyValue('padding-top').replace('px', '');

        console.log(scrollTop, Math.round($(el).offset().top));
        if (scrollTop >= Math.round($(el).offset().top + +padding - this.pseudo.offset().top) && scrollTop <= Math.round($(el).offset().top + $(el).height() + +padding - this.pseudo.offset().top)) {
          $(`.js-st-link[href="${data}"]`).addClass('is-active');
          return false;
        } else {
          $('.js-st-link').removeClass('is-active');
        }
      });
    }

    _click(e) {
      e.preventDefault();
      let data = $(e.currentTarget).attr('href');
      let target = $(`.js-sticky-scroll-block[data-id="${data}"]`);
      let padding = window.getComputedStyle(target[0], null).getPropertyValue('padding-top').replace('px', '');
      console.log(data, target, padding);

      HTMLBODY.stop().animate({
        'scrollTop': Math.round(target.offset().top + +padding - this.pseudo.offset().top)
      }, 1200, 'easeInOutCubic');
    }

  }

  DOC.ready(() => {
    new StickyNavigation();
  });
})(jQuery);
