
import { TweenLite } from 'gsap/TweenMax';
import BezierEasing from 'bezier-easing';
import imagesLoaded from 'imagesloaded';

import Parlx from 'parlx.js';

import simpleParallax from 'simple-parallax-js';
const { detect } = require('detect-browser');
const browser = detect();

(function($) {
  const WIN = $(window);
  const DOC = $(document);

  class InteractiveHeader {
    constructor() {
      this.wrapper = $('.js-interactive-header');
      this.accordion = $('.js-accordion');
      this.accordionTitle = $('.js-accordion-title');
      this.accordionContent = $('.js-accordion-content');
      this.accordionImg = $('.js-accordion-img');
      this.isAccordionMoving = false;
      this.isFirstClick = false;
      this.isFirstOpen = false;
      this.activeIndex;
      this.easing = BezierEasing(0.64, 0.04, 0.35, 1);
      this.START_DELAY = +this.wrapper.data('start-delay');
      this.PREVIEW_ANIM_DURATION = this.wrapper.data('duration')/1000;
      this._init();
    }

    _init() {
      if(!this.wrapper[0]) return;

      if(browser.name === 'ie') {
        this._addParallaxIE();
      }else{
        this._addParallax();
      }

      const isStorageLoad = this._detectStorageLoad();
      if(isStorageLoad === true) {
        this._initFormStorage();
      }else {
        setTimeout(() => {
          if(this.isFirstClick === true) return;
          if( WIN.innerWidth() > 1023 ) {
            this._previewAnimation();
          }else{
            this._openFirstAccordion();
          }
        }, this.START_DELAY);
      }

      this.accordionTitle.on('click', this._onClick.bind(this));
    }

    _detectStorageLoad() {
      const activeIndex = window.sessionStorage.getItem('interactiveHeaderActiveIndex');
      if(activeIndex !== 'null' && activeIndex !== null) {
        return true;
      }else{
        return false;
      }
    }
    
    _initFormStorage() {
      const activeIndex =  +window.sessionStorage.getItem('interactiveHeaderActiveIndex');
      this.activeIndex = activeIndex;
      this.isFirstOpen = true;
      const accordion = $(this.accordion[this.activeIndex]);
      const accordionImg = $(this.accordionImg[this.activeIndex]);
      const accordionContent = $(this.accordionContent[this.activeIndex]);
      this._open(accordion, accordionImg, accordionContent);
    }

    _setStorageData() {
      window.sessionStorage.setItem('interactiveHeaderActiveIndex', this.activeIndex);
    }

    _clearStorageData() {
      window.sessionStorage.setItem('interactiveHeaderActiveIndex', 'null');
    }

    _addParallax() {
      const elems = document.querySelectorAll('.js-parallax');
      const parlx = new Parlx(elems, {
        type: 'foreground',
        speed: -0.5,
        height: '150px'
      });

    }
    _previewAnimation() {
      // if (!this.isFirstClick) {
      this.previewTl = new TimelineMax({paused: true});
      this.accordion.each((i, el) => {
        const $el = $(el);
        if(i !== 0) {
          const prevIndex = i - 1;
          this.previewTl
            .to(this.accordion.eq(prevIndex), 0, {'className': '-=is-preview-active'}, `+=${this.PREVIEW_ANIM_DURATION}`)
            .to(this.accordionImg.eq(prevIndex), 0, {'className': '-=is-preview-active'}, '+=0');
        }

        if(i === this.accordion.length - 1 ) {
          this.previewTl
            .to($el, 0, {'className': '+=is-preview-active'}, '+=0')
            .to(this.accordionImg.eq(i), 0, {'className': '+=is-preview-active'}, '+=0')
            .to($el, this.PREVIEW_ANIM_DURATION, {onComplete: () => {this._openFirstAccordion();}}, '+=0');
        }else{
          this.previewTl
            .to(this.accordionImg.eq(i), 0, {'className': '+=is-preview-active'}, '+=0')
            .to($el, 0, {'className': '+=is-preview-active'}, '+=0');
        }


      });
      this.previewTl.play();

      // }
    }

    _addParallaxIE() {
      const elems = document.querySelectorAll('.js-parallax');
      let img = $(elems).find('img');
      $(elems).css('height', '100px');

      new simpleParallax(img, {
        overflow: true,
        scale: 1.08

      });
    }


    _openFirstAccordion() {
      if (!this.isFirstClick) {
        this.isFirstClick = true;
        this.isFirstOpen = true;
        const accordion = $(this.accordion[0]);
        const accordionImg = $(this.accordionImg[0]);
        const accordionContent = $(this.accordionContent[0]);
        this.activeIndex = 0;
        this._setStorageData();
        this._open(accordion, accordionImg, accordionContent);

      }
    }

    _open(accordion, accordionImg, accordionContent) {
      if(this.previewTl !== undefined) {
        this.previewTl.stop();
      }
      this.accordion.removeClass('is-preview-active');
      this.accordionImg.removeClass('is-preview-active');

      accordion.addClass('is-active');
      accordionImg.addClass('is-active');
      this.currentContent = accordionContent;
      this.isAccordionMoving = true;

      TweenLite.set(accordionContent, {height: 'auto', visibility: 'visible'});
      TweenLite.from(accordionContent, 0.6, {height: 0, ease: this.easing, onComplete: () => {
        this.isAccordionMoving = false;
      }});
    }

    _close() {
      this.accordion.removeClass('is-active');
      this.accordionImg.removeClass('is-active');
      this.isAccordionMoving = true;
      const currentContent = this.currentContent;
      TweenLite.to(currentContent, 0.6, {height: 0, ease: this.easing, onComplete: () => {
        this.isAccordionMoving = false;
        TweenLite.set(currentContent, {visibility: 'hidden'});
      }});
    }

    _onClick(e) {
      e.preventDefault();
      if (this.isAccordionMoving) return;
      this.isFirstClick = true;
      const accordion = $(e.currentTarget).closest('.js-accordion');
      const accordionContent = accordion.find('.js-accordion-content');
      const index = accordion.index()-1;
      const accordionImg = $(this.accordionImg[index]);
      if (!accordion.hasClass('is-active')) {
        if (!this.isFirstOpen) {
          this.isFirstOpen = true;
        } else {
          this._close();
        }
        this.activeIndex = index;
        this._setStorageData();
        this._open(accordion, accordionImg, accordionContent);
      }
      else {
        this._clearStorageData();
        this._close();
        this.isFirstOpen = false;
      }
    }
  }

  DOC.ready(() => {
    new InteractiveHeader();
  });
})(jQuery);
