import * as turf from '@turf/turf';
import PerfectScrollbar from 'perfect-scrollbar';
import { throttle, debounce } from 'throttle-debounce';
const { detect } = require('detect-browser');
const browser = detect();

(function($) {
  const DOC = $(document);
  const WIN = $(window);
  const BODY = $('body');

  class Map {
    constructor() {
      this.json = $('#map').data('json');
      this.isStorageInit = false;
      this._detect();
      this._createPopup();
      this._createGeojson();
      this._init();


    }

    _detect() {
      if (browser) {
        console.log(browser.name);
        console.log(browser.version);
        console.log(browser.os);
        if (browser.os === 'iOS' || browser.os === 'Android OS') {
          $('.js-map').css('height', WIN.height());

          WIN.on('resize', throttle(300, () => {
            $('.js-map').css('height', WIN.height());
            if (this.map) {
              this.map.resize();
            }
          }));
        }

        WIN.on('resize', throttle(300, () => {
          let popup = $('.marker-popup');
          let popupH = popup.height();
          let popupHeaderH = popup.find('.marker-popup__header').height();

          let body = popup.find('.marker-popup__body');
          let pt = body.css('padding-top');
          let pb = body.css('padding-bottom');
          let contentH = popupH - popupHeaderH - parseInt(pt)- parseInt(pb);

          $('.js-map-popup-body').css('height', `${contentH}px`);
          if (this.ps) {
            this.ps.update();
          }
        }));

      }
    }

    _createGeojson() {
      this.collection = [];

      var jqxhr = $.getJSON(this.json, data => {
        let index = 0;
        data.forEach(el => {
          index ++;
          if (index < 2) return;
          // if (el['3'] < -90 || el['3'] > 90) return;
          let point = turf.point([el[3], el[2]], {
            name: el[4],
            description: el[13],
            country_title: data[0][5],
            country_content: el[5],
            type_title: data[0][6],
            type_content: el[6],
            year_title: data[0][7],
            year_content: el[7],
            scope_title: data[0][8],
            scope_content: el[8],
            capacity_title: data[0][9],
            capacity_content: el[9],
            client_title: data[0][11],
            client_content: el[11],
            commisioning_title: data[0][12],
            commisioning_content: el[12],
            techdata_title: data[0][15],
            techdata_content: el[15],
            pdf_link: el[26],
            details_link: el[27]
          }, {id: el[1]});
          this.collection.push(point);
        });
        this.geojson = turf.featureCollection(this.collection);
      });
    }

    _createPopup() {
      this.popup = document.createElement('div');
      this.popup.className = 'map-popup';
      $('.js-map').append(this.popup);
    }

    _addList(client_title, client_content, commisioning_title, commisioning_content, techdata_title, techdata_content) {
      let currentList = '';
      if (client_content) {
        currentList += `<li><strong>${client_title}</strong>${client_content}</li>`;
      }
      if (commisioning_content) {
        currentList += `<li><strong>${commisioning_title}</strong>${commisioning_content}</li>`;
      }
      if (techdata_content) {
        currentList += `<li><strong>${techdata_title}</strong>${techdata_content}</li>`;
      }
      return currentList;
    }

    _showPopup(features) {
      $(this.popup).removeClass('map-popup--small-info');

      let isLinkPopup = features[0].properties.pdf_link !== '' ? true: false;

      let html;
      if(isLinkPopup === true) {
        html = `<div class="marker-popup ">
                    <div class="marker-popup__header">
                      <div class="marker-popup__btns">
                        <a href="${features[0].properties.details_link}" class="marker-popup__btn marker-popup__btn--case js-map-link" >
                          <span class="marker-popup__btn-text">case <br/> details</span>
                          <span class="marker-popup__btn-svg">
                            <?xml version="1.0" encoding="utf-8"?> <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="22px" height="24px" viewBox="0 0 22 24" style="enable-background:new 0 0 22 24;" xml:space="preserve"> <g> <path d="M9,9H1c-0.6,0-1,0.4-1,1s0.4,1,1,1h8c0.6,0,1-0.4,1-1S9.6,9,9,9z"/> <path d="M9,15H1c-0.6,0-1,0.4-1,1s0.4,1,1,1h8c0.6,0,1-0.4,1-1S9.6,15,9,15z"/> <path d="M5,21H1c-0.6,0-1,0.4-1,1s0.4,1,1,1h4c0.6,0,1-0.4,1-1S5.6,21,5,21z"/> <path d="M16,3H9H1C0.4,3,0,3.4,0,4s0.4,1,1,1h8h7c0.6,0,1-0.4,1-1S16.6,3,16,3z"/> <path d="M21.7,3.3l-2.8-2.8c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4L19.6,4l-2.1,2.1c-0.4,0.4-0.4,1,0,1.4c0.4,0.4,1,0.4,1.4,0 l2.8-2.8C22.1,4.3,22.1,3.7,21.7,3.3z"/> </g> </svg>
                          </span>
                        </a>
                      </div>
                      <div class="marker-popup__head">
                        <h6 class="marker-popup__title">${features[0].properties.name}</h6>
                        <p>${features[0].properties.description}</p>
                        <button class="marker-popup__close js-map-popup-close">
                          <svg class="icon icon-close-small">
                            <use xlink:href="img/sprite.svg#icon-close-small"></use>
                          </svg>
                        </button>
                      </div>
                    </div>
                    <div class="marker-popup__body">
                      <ul class="marker-popup__list js-map-popup-body">
                      <li><strong>${features[0].properties.year_title}</strong>${features[0].properties.year_content}</li>
                      <li><strong>${features[0].properties.scope_title}</strong>${features[0].properties.scope_content}</li>
                      <li><strong>${features[0].properties.capacity_title}</strong>${features[0].properties.capacity_content}</li>


                                              ${this._addList(
    features[0].properties.client_title,
    features[0].properties.client_content,
    features[0].properties.commisioning_title,
    features[0].properties.commisioning_content,
    features[0].properties.techdata_title,
    features[0].properties.techdata_content
  )}

                      </ul>
                    </div>
                  </div>`;
      }else{
        html = `<div class="marker-popup ">
                  <div class="marker-popup__header">
                    <div class="marker-popup__head">
                      <h6 class="marker-popup__title">${features[0].properties.name}</h6>
                      <!--<p class="marker-popup__mob-title-info">${features[0].properties.description}</p>-->
                      <div class="marker-popup__mob-content">
                      <ul class="marker-popup__list">
                      <li><strong>${features[0].properties.type_title}:</strong>${features[0].properties.type_content}</li>
                      <li><strong>${features[0].properties.capacity_title}:</strong>${features[0].properties.capacity_content}</li>
                    </ul>
                      </div>
                      <button class="marker-popup__close js-map-popup-close">
                        <svg class="icon icon-close-small">
                          <use xlink:href="img/sprite.svg#icon-close-small"></use>
                        </svg>
                      </button>
                    </div>
                  </div>
                  <div class="marker-popup__body">
                    <ul class="marker-popup__list js-map-popup-body">
                      <li><strong>${features[0].properties.country_title}</strong>${features[0].properties.country_content}</li>
                      <li><strong>${features[0].properties.type_title}</strong>${features[0].properties.type_content}</li>
                      <li><strong>${features[0].properties.year_title}</strong>${features[0].properties.year_content}</li>
                      <li><strong>${features[0].properties.scope_title}</strong>${features[0].properties.scope_content}</li>
                      <li><strong>${features[0].properties.capacity_title}</strong>${features[0].properties.capacity_content}</li>

                    </ul>
                  </div>
                </div>`;
        $(this.popup).addClass('map-popup--small-info');
      };

      this.popup.innerHTML = html;
      $('.js-map-link').on('click', this._handlerLinkClick.bind(this));
      $('.js-map-popup-close').on('click', e => {
        $(this.popup).removeClass('is-active');
        if (this.hoveredStateId) {
          this.map.setFeatureState({source: 'data', id: this.hoveredStateId}, { hover: false});
        }
        this.hoveredStateId = null;
      });

      this._initScrollbar();
    }
    _handlerLinkClick(e) {
      e.preventDefault();

      const $el = $(e.currentTarget);
      const href = $el.attr('href');
      this._setStorageData();
      window.open(href, '_self' );
    }

    _setStorageData() {
      //  upd zoom
      this.storageData.zoom = this.map.getZoom();
      window.sessionStorage.setItem('myMapActiveProjectId', this.storageData.id);
      window.sessionStorage.setItem('myMapZoom', this.storageData.zoom);
      window.sessionStorage.setItem('myMapCoordLng', this.storageData.coordLng);
      window.sessionStorage.setItem('myMapcoordLat', this.storageData.coordLat);
    }
    _clearStorageData() {
      window.sessionStorage.setItem('myMapActiveProjectId', 'null');
      window.sessionStorage.setItem('myMapZoom', 'null');
      window.sessionStorage.setItem('myMapCoordLng', 'null');
      window.sessionStorage.setItem('myMapcoordLat', 'null');
    }

    _initScrollbar() {
      let popup = $('.marker-popup');
      let popupH = popup.height();
      let popupHeaderH = popup.find('.marker-popup__header').height();

      let body = popup.find('.marker-popup__body');
      let pt = body.css('padding-top');
      let pb = body.css('padding-bottom');
      let contentH = popupH - popupHeaderH - parseInt(pt)- parseInt(pb);

      $('.js-map-popup-body').css('height', `${contentH}px`);
      this.ps = new PerfectScrollbar('.js-map-popup-body', {
        // scrollYMarginOffset: 40
      });
    }

    _detectStorageInit() {
      const idStr = window.sessionStorage.getItem('myMapActiveProjectId');
      // null by default, but on sessionStorage.setItem null tronsform to 'null'
      if(idStr !== 'null' && idStr !== null && $('#map')[0]) {
        this.isStorageInit = true;
      }
    }
    _getFeature(activeId) {
      const id = parseInt(activeId);
      for(let i = 0; i < this.collection.length; i++) {
        if(parseInt(this.collection[i].id) === id) {
          return this.collection[i];
          break;
        }
      }
 

    }
    _init() {
      let zoom;
      let coordLng;
      let coordLat;
      let activeId;

      this._detectStorageInit();
      if(this.isStorageInit === false) {
        zoom = 2;
        coordLng = 19.5;
        coordLat = 43.5;
      }else{
        activeId = window.sessionStorage.getItem('myMapActiveProjectId');
        zoom = +window.sessionStorage.getItem('myMapZoom');
        coordLng = +window.sessionStorage.getItem('myMapCoordLng');
        coordLat = +window.sessionStorage.getItem('myMapcoordLat');
      }

      mapboxgl.accessToken = 'pk.eyJ1Ijoib2x5YWNvZGVyaXZlciIsImEiOiJjam1haHZuN2g0eTl5M3Fxdmc3ZTRmem95In0.GsGGutWqojnVTcBcuQilxQ';
      this.map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/olyacoderiver/cjrlua2w70lga2srvd0gmrngc', // stylesheet location
        center: [coordLng, coordLat], // starting position [lng, lat]
        zoom: zoom // starting zoom
        // 1.49
      });

      this.map.on('load', () => {

        this.map.loadImage('img/map/pin.png', (error, image) => {
          if (error) throw error;
          this.map.addImage('pin', image);

          this.map.addSource('data', {
            type: 'geojson',
            // Point to GeoJSON data. This example visualizes all M1.0+ earthquakes
            // from 12/22/15 to 1/21/16 as logged by USGS' Earthquake hazards program.
            data: this.geojson,
            cluster: true,
            clusterMaxZoom: 14, // Max zoom to cluster points on
            clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
          });


          this.map.addLayer({
            id: 'clusters',
            type: 'circle',
            source: 'data',
            filter: ['has', 'point_count'],
            paint: {
              // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
              // with three steps to implement three types of circles:
              //   * Blue, 20px circles when point count is less than 100
              //   * Yellow, 30px circles when point count is between 100 and 750
              //   * Pink, 40px circles when point count is greater than or equal to 750
              'circle-color': [
                'step',
                ['get', 'point_count'],
                '#ec6b06',
                100,
                '#ec6b06',
                750,
                '#ec6b06'
              ],
              'circle-radius': [
                'step',
                ['get', 'point_count'],
                20,
                100,
                30,
                750,
                40
              ]
            }
          });

          this.map.addLayer({
            id: 'cluster-count',
            type: 'symbol',
            source: 'data',
            filter: ['has', 'point_count'],
            layout: {
              'text-field': '{point_count_abbreviated}',
              'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
              'text-size': 12
            }
          });

          this.map.addLayer({
            id: 'unclustered-point',
            type: 'circle',
            source: 'data',
            filter: ['!', ['has', 'point_count']],
            paint: {
              'circle-color': ['case',
                ['boolean', ['feature-state', 'hover'], false],
                '#000000',
                '#ec6b06'
              ],
              'circle-radius': ['case',
                ['boolean', ['feature-state', 'hover'], false],
                12,
                8
              ]
            }
          });
        });

        // inspect a cluster on click
        this.map.on('click', 'clusters', e => {
          var features = this.map.queryRenderedFeatures(e.point, { layers: ['clusters'] });
          var clusterId = features[0].properties.cluster_id;
          this.map.getSource('data').getClusterExpansionZoom(clusterId, (err, zoom) => {
            if (err)
              return;

            this.map.easeTo({
              center: features[0].geometry.coordinates,
              zoom: zoom
            });
          });
        });

        this.hoveredStateId = null;

        this.map.on('click', 'unclustered-point', e => {
          e.originalEvent.stopPropagation();
          const features = this.map.queryRenderedFeatures(e.point, { layers: ['unclustered-point'] });
          this._showPopup(features);
          if (e.features.length > 0) {
            if (this.hoveredStateId) {
              this.map.setFeatureState({source: 'data', id: this.hoveredStateId}, { hover: false});
            }

            this.hoveredStateId = features[0].id;
            this.map.setFeatureState({source: 'data', id: this.hoveredStateId}, { hover: true});
          }
          $(this.popup).addClass('is-active');
          this.storageData = {
            'id': this.hoveredStateId,
            'zoom': this.map.getZoom(),
            'coordLng': features[0].geometry.coordinates[0],
            'coordLat': features[0].geometry.coordinates[1]
          };
          this.map.flyTo({center: features[0].geometry.coordinates, speed: 0.95});
        });

        this.map.on('mouseenter', 'clusters', () => {
          this.map.getCanvas().style.cursor = 'pointer';
        });
        this.map.on('mouseleave', 'clusters', () => {
          this.map.getCanvas().style.cursor = '';
        });

        if(this.isStorageInit === true) {
          setTimeout(() => {
            this._clearStorageData();
            this.isStorageInit = false;
            const feature = this._getFeature(activeId);
            const featuresArr = [];
            // console.log(feature);
            featuresArr.push(feature);

            this._showPopup(featuresArr);


            if (this.hoveredStateId) {
              this.map.setFeatureState({source: 'data', id: this.hoveredStateId}, { hover: false});
            }
            this.hoveredStateId = +featuresArr[0].id;
            this.map.setFeatureState({source: 'data', id: this.hoveredStateId}, { hover: true});
            
            this.storageData = {
              'id': this.hoveredStateId,
              'zoom': this.map.getZoom(),
              'coordLng': featuresArr[0].geometry.coordinates[0],
              'coordLat': featuresArr[0].geometry.coordinates[1]
            };
            $(this.popup).addClass('is-active');
          }, 200);


        }
      });


      $('#map').on('click', e => {
        $(this.popup).removeClass('is-active');
        // console.log(this.hoveredStateId);
        if (this.hoveredStateId) {
          this.map.setFeatureState({source: 'data', id: this.hoveredStateId}, { hover: false});
        }
        this.hoveredStateId = null;
        // if ($(e.target).hasClass('marker') || $(e.target).closest('.marker').length !== 0) return;
        // $('.marker').removeClass('is-active');
      });



    } //init end


  }

  DOC.ready(() => {
    new Map();
  });
})(jQuery);
