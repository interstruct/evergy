import { throttle, debounce } from 'throttle-debounce';

(function($) {
  console.log('header,js');
  const WIN = $(window);
  const DOC = $(document);
  const BODY = $('body');
  const HTMLBODY = $('html, body');
    
  const HEADER = $('.header');
  const HEADERSTICKY = $('.header-sticky');
  const MAIN = $('.js-main');
  const FOOTER = $('.footer');

  const BREAKPOINTS = {
    desktopLG: window.matchMedia('(max-width: 1499px)'),
    desktopMD: window.matchMedia('(max-width: 1279px)'),
    desktopSM: window.matchMedia('(max-width: 1023px)'),
    tabletLG: window.matchMedia('(max-width: 991px)')
  };

  const PREVENTDEFAULT = e => {
    e.preventDefault();
  };


  class Global {
    constructor() {
      this.isScrolling = false;
    }


    disableScroll() {
      WIN[0].addEventListener('touchmove', PREVENTDEFAULT, { passive: false });
    }

    enableScroll() {
      WIN[0].removeEventListener('touchmove', PREVENTDEFAULT, { passive: false });
    }
  }

  let G = new Global;

  // test rebuild
  class Header {
    constructor() {
      this.burger = $('.js-burger');
      this.isLand = Math.abs(window.orientation) === 90;

      this._init();
    }

    _init() {
      this._addOpenListener();
      this._addScrollListener();

      this._cloneNavigation();

      this._onResize(BREAKPOINTS.tabletLG);
      BREAKPOINTS.tabletLG.addListener(this._onResize.bind(this));

      this._onOrientationChange();
    }

    _cloneNavigation() {
      const navigation = HEADER.find('.header__nav').clone();
      navigation.removeClass('header__nav col-dl-10 col-dl-push-1').addClass('wrapper').wrap('<div class="service-nav js-service-nav"></div>');
      this.navigation = navigation.parent();
      this.navigationInner = this.navigation.find('nav');
      this.navigationInner.wrap('<div class="row"><div class="col-tm-16 col-tm-push-11 col-mm-push-8"></div></div>');
      BODY.append(this.navigation);
    }

    _addOpenListener() {
      this.burger.on('click', e => {
        e.preventDefault();

        this.translateX = this._getTranslate();
        this.burger.toggleClass('is-active');
        this.navigation.toggleClass('is-active');
        this.navigationInner.css('padding-top', $(e.currentTarget).closest('header').height() + 30);
        BODY.toggleClass('is-fixed');

        this._slideContent();
      });

      BODY.on('click touchstart', e => {
        if (BODY.hasClass('is-fixed') &&
        $(e.target).closest('.js-service-nav nav').length === 0 &&
        !$(e.target).hasClass('.js-service-nav nav') &&
        $(e.target).closest(this.burger).length === 0 &&
        !$(e.target).hasClass(this.burger)) {
          this.burger.removeClass('is-active');
          this.navigation.removeClass('is-active');
          BODY.removeClass('is-fixed');
          G.enableScroll();
          // $('input').removeAttr('disabled');
          MAIN.css({
            'transform': 'translateX(0)'
          });

          FOOTER.css({
            'transform': 'translateX(0)'
          });
        }
      });
    }

    _getTranslate() {
      return this.navigationInner.outerWidth() + (this.navigation.width() - this.navigation.children().first().width())/2;
    }

    _slideContent() {
      if (BODY.hasClass('is-fixed')) {
        if (!this.isLand) {
          G.disableScroll();
        } else {
          G.enableScroll();
        }
        // $('input').attr('disabled', 'disabled');

        MAIN.css(
          'transform', `translateX(${-this.translateX}px)`
        );

        FOOTER.css(
          'transform', `translateX(${-this.translateX}px)`
        );

      } else {
        G.enableScroll();
        // $('input').removeAttr('disabled');

        MAIN.css({
          'transform': 'translateX(0)'
        });

        FOOTER.css({
          'transform': 'translateX(0)'
        });
      }
    }

    _addScrollListener() {
      const hh = WIN.height()/3;
      DOC.on('scroll', throttle(250, () => {
        if (!G.isScrolling) {
          if (WIN.scrollTop() > hh) {
            if (!HEADERSTICKY.hasClass('is-sticky')) {
              HEADERSTICKY.addClass('is-sticky');
            }
          }
          else {
            if (HEADERSTICKY.hasClass('is-sticky')) {
              if (HEADERSTICKY.hasClass('is-sticky')) {
                HEADERSTICKY.removeClass('is-sticky');
              }
            }
          }
        }
      }));
    }

    _onOrientationChange() {
      WIN.on('orientationchange', e => {
        this.isLand = Math.abs(window.orientation) === 90;
        this.translateX = this._getTranslate();
        this._slideContent();
      });
    }


    _onResize(point) {
      if (point.matches) {
        return;
      }
      else {
        this.burger.removeClass('is-active');
        this.navigation.removeClass('is-active');
        MAIN.removeAttr('style');
        FOOTER.removeAttr('style');
        BODY.removeClass('is-fixed');
        // G.enableScroll();
      }
    }
  }

  DOC.ready(() => {
    new Header();
  });
})(jQuery);
