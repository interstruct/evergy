import { throttle, debounce } from 'throttle-debounce';
import { TimelineMax } from 'gsap/TweenMax';
import  './_easing';


(function($) {

  return;
  console.log('helo from topBanner.js');
  const WIN = $(window);
  const DOC = $(document);
  const BODY = $('body');
  const customThrottle = function(callback) {
    let active = false; // a simple flag
    let evt; // to keep track of the last event
    let handler = function() { // fired only when screen has refreshed
      active = false; // release our flag
      callback(evt);
    };
    return function handleEvent(e) { // the actual event handler
      evt = e; // save our event at each call
      if (!active) { // only if we weren't already doing it
        active = true; // raise the flag
        requestAnimationFrame(handler); // wait for next screen refresh
      };
    };
  };
  class StickyNav {
    constructor() {
      this.el = $('.js-sticky-nav');
      this.classes = {
        'scrollBlocks': '.js-sticky-scroll-block',
        'navLinks': '.js-sticky-nav-link',
        'navLists': '.js-sticky-nav-list'
      };
      this._start();

    }

    _start() {
      if(!this.el[0]) return;
      this.navList = $(this.classes.navLists);
      // this.navListReal = navListReal.is();
      this._generateNavHtml();

    }
    _init() {
      this.isIE = BODY.hasClass('ie') ? true: false;
      this.isFF = BODY.hasClass('firefox') ? true: false;
      this.isChrome= BODY.hasClass('chrome') ? true: false;
      this.pseudo = $('.js-sticky-nav-pseudo');
      //  80 distance between nav and filter
      this.separateDistance = 80;
      this.topValue = +this.el.data('top');

      this._updateValues();
      this.curNavPos = this.startTopPos;
      $('.js-main').append(this.el);
      this.el.css({'top': `${this.startTopPos}px`, 'left': `${this.offsetLeft}px`});
      if(WIN.width() >= 1024) {
        this.el.removeClass('is-disabled');
      }

      this.links = $(this.classes.navLinks);
      this.isFixed = false;
      this.behindFilter = false;
      this.projectFilter = $('.js-filter-box');
      this.isProjectsBehaviour = this.projectFilter[0] ? true: false;
      this.filterSpecOffset = this.separateDistance + this.topValue + this.pseudoHeight;

      if(this.isProjectsBehaviour === true) {
        this.projectFilter.css({'top': `${this.filterSpecOffset}px`});
        this.projectList = $('.js-projects-list-wrapper');
        this.break = false; // fix double interval  calling edge/ie in edge
        let interval = setInterval(() => {
          if(window.projectsLoaded === true) {
            if(this.break !== false) return;
            this.break = true;
            console.log('projects loaded');
            clearInterval(interval);
            this._getNavPosState();
            // this._scroll();
            this._resize();
            if (this.isIE === false) {
              this._moreProjectsClick();
            }
          }
        },20);
      }else{
        this._getNavPosState();
        // this._scroll();
        this._resize();
      }
      this._onClick();
      this._raf();
    }

    _generateNavHtml() {
      let dataArr = [];
      $(this.classes.scrollBlocks).each((index, el) => {
        let $this = $(el);
        let obj = {'id': $this.data('id'), 'name': $this.data('name')};
        dataArr.push(obj);
      });

      this.navList.each((index, el) => {
        for(let i = 0; i < dataArr.length; i++) {
          let linkHtml = `<a href="${dataArr[i].id}" class="sticky-nav__item js-sticky-nav-link">${dataArr[i].name}</a>`;
          $(el).append(linkHtml);
        }
      });


      this._init();

    }



    _raf() {
      let self = this;
      function step() {
        let scrollPos = WIN.scrollTop() - self.offsetTop;
        self.curNavPos += (scrollPos - self.curNavPos)*0.2;

        if(self.curNavPos !== scrollPos) {
          TweenMax.to( self.el, 0.3, { y: self.curNavPos, ease: Power0.easeNone, overwrite: 5 });
        }
        requestAnimationFrame(step);
      }
      requestAnimationFrame(step);
    }


    _getNavPosState() {

      if(WIN.width() < 1024) {
        this.el.addClass('is-disabled');
        return;
      }else{
        this.el.removeClass('is-disabled');
      };

      this.el.css({'left': `${this.offsetLeft}px`});
      if(WIN.scrollTop() >= this.offsetTop) {
        // let scrollPos = WIN.scrollTop() - this.offsetTop;
        // this.curNavPos += (scrollPos - this.curNavPos)*0.8;
        // // TweenMax.to( this.el, 0.2, { css: { 'transform' : `translate3d(0px, ${scrollPos}px, 0px)` }});
        // TweenMax.to( this.el, 0.1, { y: this.curNavPos, ease: Power0.easeNone, overwrite: 5 });
        //
        return;
        // if(this.isFixed === false) {

        //   this.isFixed = true;

        //   this.el.css({'top': `${this.topValue}px`, 'position': 'fixed'});
        // }
        if(this.isProjectsBehaviour === true) {
          let projectListOffsetTop = this.projectList.offset().top;
          let filterHeight = this.projectFilter.innerHeight();
          let filterOffset;

          if(this.isIE === true) {
            filterOffset = projectListOffsetTop - WIN.scrollTop() - this.filterSpecOffset;
          }else{
            filterOffset = projectListOffsetTop + this.projectList.innerHeight() - WIN.scrollTop() - filterHeight - this.filterSpecOffset;
          }

          if(filterOffset <= 0 ) {
            if(this.behindFilter === false) {
              let newPos;
              if(this.isIE === true) {
                newPos = Math.round(filterOffset + WIN.scrollTop() + this.topValue);
              }else{
                // if(this.isFF === true) {
                //   newPos = Math.round(filterOffset + WIN.scrollTop() + this.topValue - this.headersHeight);
                // }else{
                newPos = Math.round(filterOffset + WIN.scrollTop() + this.topValue - this.headersHeight);
                // }

              }
              // this.el.addClass('fixed').css({'top': `${newPos}px`, 'left': `${this.offsetLeft}px`, 'position': 'absolute'});
              this.el.addClass('fixed').css({'top': `${newPos}px`, 'position': 'absolute'});
              this.behindFilter = true;
            }
          }else{
            if(this.behindFilter === true) {
              // this.el.addClass('fixed').css({'top': `${this.topValue}px`, 'left': `${this.offsetLeft}px`, 'position': 'fixed'});
              this.el.addClass('fixed').css({'top': `${this.topValue}px`, 'position': 'fixed'});
              this.behindFilter = false;
            }
          }

        }
        //
      }else{
        // if(this.isFixed === true) {

        //   this.isFixed = false;

        //   this.el.css({'top': `${this.startTopPos}px`, 'left': `${this.offsetLeft}px`, 'position': 'absolute'});

        // }

      }

    }

    _scroll() {
      this.throttleFunc = throttle(10, this._scrollFunc.bind(this));
      WIN.on('scroll', this.throttleFunc);
      // let self = this;
      if(this.isChrome === true) {
        console.log('chrome scrolling');
        // window.addEventListener('scroll', customThrottle(this._scrollFunc.bind(this)));
        // WIN.on('scroll', this._scrollFunc.bind(this));
      }else{
        // this.throttleFunc = throttle(10, this._scrollFunc.bind(this));
        // WIN.on('scroll', this.throttleFunc);

        WIN.on('scroll', this._scrollFunc.bind(this));


        // window.addEventListener('scroll', customThrottle(this._scrollFunc.bind(this)));
      }




    }
    _scrollFunc(e) {
      if(WIN.width() < 1024) return;

      this._getNavPosState();
      let scrollPos = WIN.scrollTop();
      $(this.classes.scrollBlocks).each((i, el) => {
        let $this  = $(el);
        let headerHeight = $('.js-headers').find('.header-sticky').innerHeight();


        if ($this.offset().top - headerHeight <= scrollPos && $this.offset().top + $this.innerHeight() > scrollPos + headerHeight) {
          this.el.find('a').removeClass('is-active');
          let data = $this.data('id');
          // console.log($(`${this.classes.navLinks}[href="${data}"]`));
          this.el.find(`${this.classes.navLinks}[href="${data}"]`).addClass('is-active');
          return false;
        }else{
          this.el.find('a').removeClass('is-active');
        }
      });

    }

    _onClick() {
      // let self = this;
      this.links.on('click', (e) => {
        e.preventDefault();
        let $this = $(e.target);
        // if($this.hasClass('is-active')) return;
        let target =  $(`${this.classes.scrollBlocks}[data-id="${$this.attr('href')}"]`);
        this.el.find('a').removeClass('is-active');

        let headerHeight = $('.js-headers').find('.header-sticky').innerHeight();

        $('html, body').stop().animate({
          'scrollTop': target.offset().top - headerHeight + 1
        }, 1200, 'easeInOutCubic', function() {
          // WIN.on('scroll', self.throttleFunc);
        });
      });
    }

    _resize() {
      let self = this;
      this.resizeFunc = throttle(300, () => {
      // this.resizeFunc = debounce(350, () => {
        // if(WIN.width() >= 1024) {
        self._updateValues(true);
        // }
      });
      WIN.on('resize', this.resizeFunc);
    }
    _updateValues(callNavStateFunc) {

      // this.offsetTop = this.pseudo.offset().top - this.topValue;
      // this.offsetLeft = this.pseudo.offset().left;
      // this.pseudoHeight = this.pseudo.innerHeight();
      // this.headersHeight = $('.js-headers').innerHeight();

      // console.log('update');
      this.offsetTop = this.pseudo.offset().top - this.topValue;
      this.offsetLeft = this.pseudo.offset().left;
      this.pseudoHeight = this.pseudo.innerHeight();
      this.headersHeight = $('.js-headers').innerHeight();
      this.filterSpecOffset = this.separateDistance + this.topValue + this.pseudoHeight;
      // this.startTopPos = this.pseudo.offset().top - this.headersHeight;
      if(this.isIE === true) {
        this.startTopPos = this.pseudo.offset().top;
      }else{
        this.startTopPos = this.pseudo.offset().top - this.headersHeight;
      }
      if(callNavStateFunc === true) {
        this._getNavPosState();
      }
    }
    _moreProjectsClick() {
      $('.js-more-btn').on('click', (e) => {
        e.preventDefault();
        let interval = setInterval(() => {
          if(window.projectsMoreClicked === true) {
            window.projectsMoreClicked = false;
            clearInterval(interval);
            this._getNavPosState();
            console.log('more click from topBanner.js');
          }
        },50);
      });
    }



  }
  DOC.ready(() => {

    DOC.StickyNav = new StickyNav();

  });
})(jQuery);
