import { TweenLite } from 'gsap/TweenMax';
import BezierEasing from 'bezier-easing';
import { throttle, debounce } from 'throttle-debounce';
(function($) {
  const WIN = $(window);
  const DOC = $(document);

  class JobTeaser {
    constructor() {
      this.bg = $('.js-job-teaser-bg');

      this.top_of_element;
      this.bottom_of_element;
      this.top_of_screen;
      this.bottom_of_screen;

      this.lastScrollTop = 0;
      this._init();
    }

    _init() {
      if(!this.bg[0]) return;
      this.el = this.bg.parent();
      this.banner = this.bg.parents('.topbanner');

      let offsetTop = this.banner.offset().top;
      this.el.addClass('is-animate');
      // if( offsetTop < WIN.innerHeight()) {
      //   this.el.addClass('is-animate');
      // }else{
      //   this._updateValues();
      //   this._resize();
      //   this._scroll();
      // }

    }
    _scroll() {
      this.throttleFunc = throttle(100, this._scrollFunc.bind(this));
      $(window).on('scroll', this.throttleFunc);
    }
    _resize() {
      let self = this;
      this.resizeFunc = throttle(300, () => {

        self._updateValues();
      });
      WIN.on('resize', this.resizeFunc);
    }
    _updateValues() {
      this.top_of_element = this.el.offset().top;
      this.bottom_of_element = this.el.offset().top + this.el.outerHeight();
      // this.top_of_screen = WIN.scrollTop();
      // this.bottom_of_screen = WIN.scrollTop() + WIN.innerHeight();
    }
    _scrollFunc(e) {
      let scrollTop = WIN.scrollTop();
      let windowH = WIN.innerHeight();
      let offset1 = 0;
      let offset2 = 0;
      if (scrollTop > this.lastScrollTop) {
        // downscroll code
        offset1 = -windowH/3;
      } else {
        offset2 = windowH/3;
      }
      this.lastScrollTop = scrollTop;


      this.top_of_screen = scrollTop + offset2;
      this.bottom_of_screen = scrollTop + windowH + offset1;
      if ((this.bottom_of_screen > this.top_of_element) && (this.top_of_screen < this.bottom_of_element)) {

        this.el.addClass('is-animate');

        WIN.unbind('scroll', this.throttleFunc);
        WIN.unbind('resize', this.resizeFunc);
        // the element is visible, do something
      } else {
        // the element is not visible, do something else
      }
    }

  }

  DOC.ready(() => {
    new JobTeaser();
  });
})(jQuery);
