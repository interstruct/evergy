import { TweenLite } from 'gsap/TweenMax';
import BezierEasing from 'bezier-easing';

(function($) {
  const WIN = $(window);
  const DOC = $(document);

  class Job {
    constructor() {
      this.accordion = $('.js-job');
      this.accordionTitle = $('.js-job-title');
      this.accordionContent = $('.js-job-content');
      this.isAccordionMoving = false;
      // this.isFirstClick = false;
      this.isFirstOpen = false;
      this.easing = BezierEasing(0.64, 0.04, 0.35, 1);

      this._init();
    }

    _init() {
      if(!this.accordion[0]) return;
      this.accordionTitle.on('click', this._onClick.bind(this));
      this._detectDefaultOpen();
      this._initSocials();

    }
    _detectDefaultOpen() {
      let activeItem;
      this.accordion.each((i,el) => {
        let $this = $(el);
        if($this.hasClass('is-active')) {
          activeItem = $this;
          return false;
        }
      });
      if(activeItem !== undefined) {
        let accordionContent = activeItem.find('.js-job-content');
        this._open(activeItem, accordionContent, true);
        this.isFirstOpen = true;
      }

    }


    _open(accordion, accordionContent, isDefaultOpen) {
      let time = isDefaultOpen === true ? 0: 0.6;
      accordion.addClass('is-active');
      this.currentContent = accordionContent;
      this.isAccordionMoving = true;
      TweenLite.set(accordionContent, {height: 'auto', visibility: 'visible'});
      TweenLite.from(accordionContent, time, {height: 0, ease: this.easing, onComplete: () => {
        this.isAccordionMoving = false;
      }});
    }

    _close() {
      this.accordion.removeClass('is-active');
      this.isAccordionMoving = true;
      const currentContent = this.currentContent;
      TweenLite.to(currentContent, 0.6, {height: 0, ease: this.easing, onComplete: () => {
        this.isAccordionMoving = false;
        TweenLite.set(currentContent, {visibility: 'hidden'});
      }});
    }

    _onClick(e) {
      e.preventDefault();
      if (this.isAccordionMoving) return;

      // this.isFirstClick = true;
      const accordion = $(e.currentTarget).closest('.js-job');
      const accordionContent = accordion.find('.js-job-content');
      const index = accordion.index()-1;
      if (!accordion.hasClass('is-active')) {
        if (!this.isFirstOpen) {
          this.isFirstOpen = true;
        } else {
          this._close();
        }

        this._open(accordion, accordionContent);
      }
      else {
        this._close();
        this.isFirstOpen = false;
      }
    }




    _initSocials() {
      this._initTwetter();
      this._initLinkedIn();
      this._initXing();
    }
    _initTwetter() {
      let twitterBtns = $('.js-twitter-share');

      twitterBtns.each((index, el) => {
        let btn = $(el);
        let url = this._getUrl(btn);
        let title = this._getTitle(btn);
        let shareUrl = 'https://twitter.com/intent/tweet?url=' + url+ '&text=' + title;
        btn.on('click', (e) => {
          e.preventDefault();
          let win = window.open(shareUrl, 'ShareOnTwitter', this._getSizes());
          win.opener = null;
        });
      });
    }

    _initLinkedIn() {
      let linkedBtns = $('.js-linkedin-share');

      linkedBtns.each((index, el) => {
        let btn = $(el);

        let url = this._getUrl(btn);
        let title = this._getTitle(btn);
        // let shareUrl = 'https://www.linkedin.com/shareArticle?url=' + url+ '&title=' + title;
        // let shareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${url}&title=${title}` ;
        // let shareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${url}` ;
        let shareUrl = 'https://www.linkedin.com/sharing/share-offsite/?url=' + url ;
        // console.log(shareUrl);

        // let shareUrl = 'https://www.linkedin.com/shareArticle?url=' + str ;
        // btn[0].href = shareUrl;

        btn.on('click', (e) => {
          e.preventDefault();
          let win = window.open(shareUrl, 'ShareOnLinkedIn', this._getSizes());
          win.opener = null;
        });
      });


    }
    _initXing() {
      let btns = $('.js-xing-share');
      btns.each((index, el) => {
        let btn = $(el);
        let btnHref = btn.attr('href');
        let tampStr = btnHref === '#' ? location.href : btnHref;
        let str = encodeURIComponent(tampStr);
        // console.log(str);
        let shareUrl = 'https://www.xing.com/spi/shares/new?url=' + str ;

        btn.on('click', (e) => {
          e.preventDefault();
          let win = window.open(shareUrl, 'ShareOnXing', this._getSizes());
          win.opener = null;
        });
      });

      // ;(function(d, s) {
      //   var x = d.createElement(s),
      //     s = d.getElementsByTagName(s)[0];
      //   x.src = 'https://www.xing-share.com/plugins/share.js';
      //   s.parentNode.insertBefore(x, s);
      // })(document, 'script');
    }
    _getUrl(btn) {
      return encodeURIComponent(btn.attr('href').replace( /#/, '' ));
    }
    _getTitle(btn) {
      return encodeURIComponent(btn.parents('.js-job').find('.js-job-title').text() + '\n');
    }
    _getSizes() {
      let width = 700;
      let height = 500;
      let left = (window.innerWidth / 2) - (width / 2);
      let top = (window.innerHeight / 2) - (height / 2);

      return [
        'resizable,scrollbars,status',
        'height=' + height,
        'width=' + width,
        'left=' + left,
        'top=' + top,
      ].join();
    }
  }

  DOC.ready(() => {
    new Job();
  });
})(jQuery);
