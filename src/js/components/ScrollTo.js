import { HTMLBODY } from './../global';
import { G } from './../G';

export default class ScrollTo {
  constructor() {
    this.url = $('a');
    this._init();
  }

  _init() {
    this.url.on('click', this._onClick.bind(this));
  }

  _onClick(e) {
    const url = e.currentTarget.getAttribute('href');
    if (/^#/.test(url) === true) {
      G.isScrolling = true;
      HTMLBODY.animate({
        scrollTop: $(url).offset().top
      }, 700, 'swing',
      () => {
        G.isScrolling = false;
      });
    }
  }
}
