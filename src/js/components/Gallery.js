// import './../lib/assignPolyfill';
import '@babel/polyfill';
import imagesLoaded from 'imagesloaded';
import { TweenMax } from 'gsap/TweenMax';
import { throttle, debounce } from 'throttle-debounce';

(function($) {

  const WIN = $(window);
  const DOC = $(document);
  const HTMLBODY = $('html, body');
  const HEADER = $('.header');
  const BREAKPOINTS = {
    desktopLG: window.matchMedia('(max-width: 1499px)'),
    desktopMD: window.matchMedia('(max-width: 1279px)'),
    desktopSM: window.matchMedia('(max-width: 1023px)'),
    tabletLG: window.matchMedia('(max-width: 991px)')
  };

  class Global {
    constructor() {
      this.isScrolling = false;
    }


    disableScroll() {
      WIN[0].addEventListener('touchmove', PREVENTDEFAULT, { passive: false });
    }

    enableScroll() {
      WIN[0].removeEventListener('touchmove', PREVENTDEFAULT, { passive: false });
    }
  }

  let G = new Global;

  class ScrollTo {
    constructor() {
      this.url = $('a');
      this._init();
    }

    _init() {
      this.url.on('click', this._onClick.bind(this));
    }

    _onClick(e) {
      const url = e.currentTarget.getAttribute('href');
      if (/^#/.test(url) === true) {
        if(url.length <= 1) return;
        e.preventDefault();
        G.isScrolling = true;
        TweenMax.to(HTMLBODY, 1.2, {
          scrollTop: $(url).offset().top,
          ease: Expo.easeInOut,
          onComplete: () => {
            G.isScrolling = false;
          }
        });
      }
    }
  }


  class Gallery {
    constructor() {
      this.gallery = $('.js-gallery');
      this.navigation = $('.gallery__navigation');

      this._init();
    }

    _init() {
      this._count();
      WIN.on('resize', throttle(200, () => {
        this._count();
      }));
      WIN.on('orientationchange', throttle(200, () => {
        this._count();
      }));
    }

    _count() {
      // this.gallery.css('paddingTop', HEADER.height());
      this.navigation.css('marginTop', HEADER.height()/2);
      if (WIN.height() > WIN.width() && BREAKPOINTS.desktopSM.matches) {
        console.log('port');
        this.gallery.css('height', WIN.width()-HEADER.height());
      }
      if (WIN.height() < WIN.width() && BREAKPOINTS.desktopSM.matches) {
        console.log('land');
        this.gallery.css('height', WIN.width()-HEADER.height());
      }
      else {
        this.gallery.css('height', WIN.height()-HEADER.height());
      }
    }
  }

  class Slide {
    constructor(el, settings) {
      this.DOM = {el: el};

      // The slideÂ´s container.
      this.DOM.wrap = this.DOM.el.querySelector('.slide__wrap');
      // The image element.
      this.DOM.img = this.DOM.wrap.querySelector('.slide__img');
      // The title container.
      this.DOM.titleWrap = this.DOM.wrap.querySelector('.slide__in');
      // Some config values.
      this.config = {
        animation: {
          duration: 1.2,
          ease: Expo.easeInOut
        }
      };
    }
    // Sets the current class.
    setCurrent(isCurrent = true) {
      this.DOM.el.classList[isCurrent ? 'add' : 'remove']('is-active');
    }
    // Hide the slide.
    hide(direction) {
      return this.toggle('hide', direction);
    }
    // Show the slide.
    show(direction) {
      this.DOM.el.style.zIndex = 1000;
      return this.toggle('show', direction);
    }
    // Show/Hide the slide.
    toggle(action, direction) {
      return new Promise((resolve, reject) => {
      // When showing a slide, the slideÂ´s container will move 100% from the right or left depending on the direction.
      // At the same time, both title wrap and the image will move the other way around thus creating the unreveal effect.
      // Also, when showing or hiding a slide, we scale it from or to a value of 1.1.
        if ( action === 'show' ) {
          TweenMax.to(this.DOM.wrap, this.config.animation.duration, {
            ease: this.config.animation.ease,
            startAt: {x: direction === 'right' ? '100%' : '-100%'},
            x: '0%'
          });
          TweenMax.to(this.DOM.titleWrap, this.config.animation.duration, {
            ease: this.config.animation.ease,
            startAt: {x: direction === 'right' ? '-100%' : '100%'},
            x: '0%'
          });
        }

        TweenMax.to(this.DOM.img, this.config.animation.duration, {
          ease: this.config.animation.ease,
          startAt: action === 'hide' ? {} : {x: direction === 'right' ? '-100%' : '100%', scale: 1.1},
          x: '0%',
          scale: action === 'hide' ? 1.1 : 1,
          onStart: () => {
            this.DOM.img.style.transformOrigin = action === 'hide' ?
              direction === 'right' ? '100% 50%' : '0% 50%':
              direction === 'right' ? '0% 50%' : '100% 50%';
            this.DOM.el.style.opacity = 1;
          },
          onComplete: () => {
            this.DOM.el.style.zIndex = 999;
            this.DOM.el.style.opacity = action === 'hide' ? 0 : 1;
            resolve();
          }
        });
      });
    }
  }

  class Navigation {
    constructor(el, settings) {
      this.DOM = {el: el};

      this.settings = {
        next: () => {return false;},
        prev: () => {return false;}
      };
      Object.assign(this.settings, settings);

      // Navigation controls (prev and next)
      this.DOM.child = this.DOM.el.querySelector('.gallery__navigation-in');
      this.DOM.prevCtrl = this.DOM.el.querySelector('.gallery__prev');
      this.DOM.nextCtrl = this.DOM.el.querySelector('.gallery__next');
      this.initEvents();
    }


    _onResize(point, val, height) {
      if (point.matches) {
        this.toggle(val, height);
      }
      else {
        TweenMax.set(this.DOM.child, {
          y: 0
        });
      }
    }
    toggle(val, height) {
      if (val !== 1) {
        $(this.DOM.el).addClass('is-left');
      } else {
        $(this.DOM.el).removeClass('is-left');
      }
    }

    changeMobilePosition(val, height) {
      const resize = () => {
        this._onResize(BREAKPOINTS.desktopMD, val, height);
        if (BREAKPOINTS.desktopMD.matches) {
          return;
        }
        else {
          TweenMax.set(this.DOM.child, {
            y: 0
          });
        }
      };
      this._onResize(BREAKPOINTS.desktopLG, val, height);
      BREAKPOINTS.desktopLG.addListener(resize);
    }

    // Updates the current page element value.
    setCurrent(val, total, direction) {
      this.currentSlide = val;
      if (val === 1) {
        TweenMax.to(this.DOM.prevCtrl, 1.2, {
          ease: Expo.easeInOut,
          opacity: 0,
          onComplete: () => this.DOM.prevCtrl.classList.add('is-hidden')
        });
      }
      else {
        TweenMax.to(this.DOM.prevCtrl, 1.2, {
          onStart: () => this.DOM.prevCtrl.classList.remove('is-hidden'),
          ease: Expo.easeInOut,
          opacity: 1
        });
      }
    }
    // Initialize the events on the next/prev controls.
    initEvents() {
      this.DOM.prevCtrl.addEventListener('click', () => this.settings.prev());
      this.DOM.nextCtrl.addEventListener('click', () => this.settings.next());
    }
  }


  class Badge {
    constructor(el) {
      this.DOM = {el: el};
      this.height = $(this.DOM.el).height();
      this._onResize();
    }

    _onResize() {
      WIN.on('resize', throttle(300, e => {
        this.height = $(this.DOM.el).find('.row').height();
        $(this.DOM.el).css('height', this.height);
      }));
    }

    getHeight() {
      return this.height;
    }

    toggle(val) {
      if (val !== 1) {
        this.DOM.el.classList.add('is-visible');
        TweenMax.to(this.DOM.el, 1.2, {
          ease: Expo.easeInOut,
          height: this.height,
          opacity: 1
        });
      } else {
        TweenMax.to(this.DOM.el, 1.2, {
          ease: Expo.easeInOut,
          height: 0,
          opacity: 0,
          onComplete: () => this.DOM.el.classList.remove('is-visible'),
        });
      }
    }
  }

  class Slideshow {
    constructor(el) {
      this.DOM = {el: el};
      // Initialize the navigation instance. When clicking the next or prev ctrl buttons, trigger the navigate function.
      this.navigation = new Navigation(document.querySelector('.gallery__navigation'), {
        next: () => this.navigate('right'),
        prev: () => this.navigate('left')
      });

      $(this.DOM.el).swipe({
        swipe: (event, direction, distance, duration, fingerCount, fingerData) => {
          if (direction === 'left') this.navigate('right');
          if (direction === 'right') this.navigate('left');
        },
        allowPageScroll: 'vertical'
      });

      if (document.querySelector('.js-top-badge')) {
        this.badge = new Badge(document.querySelector('.js-top-badge'));
      }
      // The slides.
      this.slides = [];
      // Initialize/Create the slides instances.
      Array.from(this.DOM.el.querySelectorAll('.slide')).forEach((slideEl,pos) => this.slides.push(new Slide(slideEl)));
      // The total number of slides.
      this.slidesTotal = this.slides.length;
      // Set the total number of slides in the navigation box.
      // this.navigation.setTotal(this.slidesTotal);
      // At least 2 slides to continue...
      if ( this.slidesTotal < 2 ) {
        return false;
      }
      // Current slide position.
      this.current = 0;
      // Initialize the slideshow.
      this.init();
    }
    // Set the current slide and initialize some events.
    init() {
      new Gallery();
      this.slides[this.current].setCurrent();
    }
    // Navigate the slideshow.
    navigate(direction) {
    // If animating return.
      if ( this.isAnimating ) return;
      this.isAnimating = true;

      // The next/prev slideÂ´s position.
      const nextSlidePos = direction === 'right' ?
        this.current < this.slidesTotal-1 ? this.current+1 : 0 :
        this.current > 0 ? this.current-1 : this.slidesTotal-1;

      this.navigation.setCurrent(nextSlidePos+1, this.slidesTotal, direction);
      if (this.badge) {
        this.badge.toggle(nextSlidePos+1);
        this.navigation.changeMobilePosition(nextSlidePos+1, this.badge.getHeight());
      }


      Promise.all([this.slides[this.current].hide(direction), this.slides[nextSlidePos].show(direction)])
        .then(() => {
        // Update current.
          this.slides[this.current].setCurrent(false);
          this.current = nextSlidePos;
          this.slides[this.current].setCurrent();
          this.isAnimating = false;
        });
    }
  }

  // Preload all the images..
  if ($('.slide__img').length !== 0) {
    imagesLoaded(document.querySelectorAll('.slide__img'), {background: true}, () => {
      document.querySelector('.js-gallery').classList.remove('is-loading');
    });
  }

  DOC.ready(() => {
    new ScrollTo();
    if (document.querySelector('.slideshow')) new Slideshow(document.querySelector('.slideshow'));
  });

})(jQuery);
