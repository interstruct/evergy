import { TimelineMax } from 'gsap/TweenMax';
import { throttle, debounce } from 'throttle-debounce';
import  './_easing';

(function($) {
  const WIN = $(window);
  const DOC = $(document);
  const HTMLBODY = $('html, body');

  class Projects {
    constructor() {
      this.classes = {
        wrapper: '.js-projects',
        filterBtn: '.js-filter-btn',
        moreBtn: '.js-more-btn',
        list: '.js-projects-list',
        filterBox: '.js-filter-box'
      };
      this._init();
    }

    _init() {
      this.wrapper = $(this.classes.wrapper);
      if(!this.wrapper[0]) return;
      this.projectPageId = this.wrapper.is('[data-projects-page-id]') ? `${this.wrapper.data('projects-page-id')}` : 'main';
      this.projectsPageType = this.wrapper.hasClass('is-simple-projects') ? 'simple': 'hard';
      this.list = this.wrapper.find(this.classes.list);
      this.moreBtn = this.wrapper.find(this.classes.moreBtn);
      this.filterBtn = this.wrapper.find(this.classes.filterBtn);
      this.filterBox = this.wrapper.find(this.classes.filterBox);
      this.jsonUrl = this.wrapper.data('json');
      this.canDoRequest = true;
      this.wpRequest = Boolean(this.wrapper.data('wp-request'));
      this.readyElemsArr = [];
      this.isIE = $('body').hasClass('ie');
      this.postPage = 1;
      this.scrollOffset;
      this.requestType = this.filterBtn.hasClass('is-active') ? this.filterBtn.filter('.is-active').data('filter-type') :'all';
      this.svgPV = `<?xml version="1.0" encoding="utf-8"?>
            <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            	 viewBox="0 0 322 177" enable-background="new 0 0 322 177" xml:space="preserve">
            <g id="icons_x2F_cases_x2F_pv">
            	<path id="PV" d="M57.9,102.6H41.7V140h-9.1V41.6h23.7c11.5,0,20.5,2.9,27.1,8.7s9.9,13.4,9.9,22.8c0,8.2-3.2,15.1-9.6,20.9
            		C77.3,99.8,68.7,102.6,57.9,102.6z M57.4,49.6H41.7v45H59c7.7,0,13.8-2,18.3-6.1c4.5-4.1,6.8-9.2,6.8-15.3c0-7.2-2.3-12.9-7-17.2
            		C72.4,51.7,65.8,49.6,57.4,49.6z M151.5,140h-12.4l-33.4-98.4h9.8l13,39l16.9,51.4l17.5-51.4l13.4-39h9.8L151.5,140z"/>
            </g>
            </svg>
            `;
      this.svgW = `<?xml version="1.0" encoding="utf-8"?>
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            	 viewBox="0 0 322 177" style="enable-background:new 0 0 322 177;" xml:space="preserve">
            <g id="icons_x2F_cases_x2F_w">
            	<path id="W" d="M64.8,140H52.4L26.8,41.6h9.5l9.2,36.5l13.3,52.7c6.5-23.3,11.5-40.8,15-52.7l10.2-35h10.7l10.9,35.2l16.4,53.1
            		c5.4-24.9,9.2-42.6,11.5-53l8.2-36.8h9.1L128.5,140H116l-9.8-31.1l-16.6-54c-3.7,13.4-8.9,31.4-15.6,54L64.8,140z"/>
            </g>
            </svg>
            `;


      this._setBtnsEvents();
      // this._getSizesForScroll();
      this._resize();
      this._findElemInStorage();
    }


    _findElemInStorage() {
      const html = window.sessionStorage.getItem('projectsHtmlString');
      // null by default, but on sessionStorage.setItem null tronsform to 'null'
      if(html !== 'null' && html !== null && this.wrapper[0]) {
        const projectPageId =  '' + window.sessionStorage.getItem('projectsPageId');
        if(projectPageId === this.projectPageId) {
          // if(this.projectsPageType) return;
          const postPage = window.sessionStorage.getItem('projectsPostPage');
          const activeRequestType = window.sessionStorage.getItem('projectsActiveRequestType');
          const isNoMoreBlocks =  window.sessionStorage.getItem('projectsIsNoMoreBlocks');
          this.list.html(html);
          this._clearStorageData();
          this._setRequestTypeFromStorage(activeRequestType);
          this.postPage = +postPage;
          this._setActiveFilterFromStorage();
          if(isNoMoreBlocks === 'true') {
            this._hideMoreBtn();
          }
        }

    
      }
    }
    _setRequestTypeFromStorage(activeRequestType) {
      this.requestType = activeRequestType;
    }

    _setActiveFilterFromStorage() {
      this.filterBtn.removeClass('is-active');
      if(this.requestType === 'PV') {
        $(`.js-filter-btn[data-filter-type="${this.requestType}"]`).addClass('is-active');
      }else if(this.requestType === 'Wind') {
        $(`.js-filter-btn[data-filter-type="${this.requestType}"]`).addClass('is-active');
      }
    }
    _setBtnsEvents() {
      this.moreBtn.on('click', (e) => {
        e.preventDefault();
        if(this.canDoRequest === false) return;
        this.btnClicked = e.currentTarget;
        this._request();
      });

      this.filterBtn.on('click', (e) => {
        e.preventDefault();
        if(this.canDoRequest === false) return;
        let type = $(e.currentTarget).data('filter-type');
        if(type === this.requestType) return;
        this.filterBtn.removeClass('is-active');
        $(e.currentTarget).addClass('is-active');

        this._request(type);
      });

      // if(this.projectsPageType) {
      $(document).on('click', '.js-projects-list .project', this._handlerClick.bind(this));
      // }else{
      //   $(document).on('click','.js-projects-list .project', this._setStorageData.bind(this));
      // }

    }
    _handlerClick(e) {
      e.preventDefault();
      const $el = $(e.currentTarget);
      if($el.hasClass('is-active')) return;
      this._clearStorageData();
      this._clearMapStorageData();
      const href = $el.attr('href');
      const htmlString = this.list.html();
      window.sessionStorage.setItem('projectsHtmlString', htmlString);
      window.sessionStorage.setItem('projectsPostPage', this.postPage);
      window.sessionStorage.setItem('projectsActiveRequestType', this.requestType);
      window.sessionStorage.setItem('projectsIsNoMoreBlocks', this.isNoMoreBlocks);
      window.sessionStorage.setItem('projectsPageId', this.projectPageId);
      this._getSizesForScroll(true, 0);
      window.open(href, '_self' );
    }

    _clearStorageData() {
      window.sessionStorage.setItem('projectsHtmlString', 'null');
      window.sessionStorage.setItem('projectsPostPage', 'null');
      window.sessionStorage.setItem('projectsActiveRequestType', 'null');
      window.sessionStorage.setItem('projectsIsNoMoreBlocks', 'null');
      window.sessionStorage.setItem('projectsPageId', 'null');
    }
    _clearMapStorageData() {
      window.sessionStorage.setItem('myMapActiveProjectId', 'null');
      window.sessionStorage.setItem('myMapZoom', 'null');
      window.sessionStorage.setItem('myMapCoordLng', 'null');
      window.sessionStorage.setItem('myMapcoordLat', 'null');
    }
    _request(newType) {
      if(this.canDoRequest !== true) return;
      this.canDoRequest = false;
      let self = this;
      let isSimpleShow;
      if(newType !== undefined) {
        isSimpleShow = false;
        this.requestType = newType;
        this.postPage = 1;
      }else{
        isSimpleShow = true;
        this.postPage++;
      }

      if(this.projectsPageType === 'simple') {
        this.postPage = -1;
      }
      if(this.wpRequest === true) {

        let data = {
          action: 'projectjson', // Do not change this line. Wordpress Magic.
          page: this.postPage, // Here number of page, starts with 1.
          //posts_per_page: 3, // Here number of posts per page.
          type: this.requestType // Here can be 1 of 2 possible value. PV or Wind.
        };
        if(this.projectsPageType === 'simple') {
          data.activeProject = this.list.data('active-project');
        }
        jQuery.post(myajax.url, data, function(response) { // Do not change this line. WordPress Magic.
          self.requestData = JSON.parse(response);
          self._generateList(isSimpleShow);
        });
      }else{
        $.ajax({
          url:  this.jsonUrl,
          dataType: 'json',
          data: this.requestType,
          success: (data) => {
            this.requestData = data;
            this._generateList(isSimpleShow);

          }
        });
      }
    }

    _generateList(isSimpleShow) {
      for (let i = 0; i < this.requestData.length; i++) {
        let item = this._generateListItem(this.requestData[i]);
        this.readyElemsArr.push($(item));
        if(i === this.requestData.length - 1) {
          this.isNoMoreBlocks = this.requestData[i].last === 1;
          // this._clearHtml();
          if(isSimpleShow === true) {
            this.isNoMoreBlocks ? this._insertNewBlocks(true) : this._insertNewBlocks(false);
          }else{
            this.isNoMoreBlocks ? this._changeAnimationCase(true): this._changeAnimationCase(false);
          }
        }
      }
    }

    _animateMoreBtn(hide) {
      if(hide) {
        this.moreBtn.fadeOut(300);
      }else{
        this.moreBtn.fadeIn(300);
      }
    }
    _hideMoreBtn() {
      this.moreBtn.fadeOut(0);
    }

    _changeAnimationCase(bool) {
      this.list.fadeOut(300, () => {
        this._clearHtml();
        this.list.fadeIn(0, () => {
          this._insertNewBlocks(bool);
          this._getSizesForScroll(true);

        });

      });

    }
    _insertNewBlocks(bool) {
      let tl = new TimelineMax({paused: true});
      let windowWidth =  $(window).width();
      let arrLength = this.readyElemsArr.length;
      let duration = windowWidth > 1024 ? 0.3 : 0.2;
      for (let i = 0; i < arrLength; i++) {
        let elem =  this.readyElemsArr[i];
        // elem.addClass('prepared');
        this.list.append(this.readyElemsArr[i]);
        let delay = i === 0 ? 0 : '-=0.15';
        let isLast = i === arrLength - 1 ? true: false;
        tl
          .fromTo(
            elem,
            duration,
            {
              y: 20,
              opacity: 0
            },
            {
              y: 0,
              opacity: 1,
              onComplete: () => {
                elem.removeClass('prepared');
                if(isLast) {
                  this.readyElemsArr.length = 0;
                  this.canDoRequest = true;
                  window.projectsLoaded = true;


                  this._animateMoreBtn(bool);
                }
              }
            },
            delay
          );

        if(isLast) {
          if($(this.btnClicked)[0] && $(this.btnClicked).hasClass('js-more-btn')) {
            this.btnClicked = null;
            window.projectsMoreClicked = true;
          }
          tl.play();


        }
      }
    }

    _clearHtml() {
      this.list.html('');
    }

    _generateListItem(data) {
      let isSpecial = data.type === 'spec' ? true : false;
      let isActive = data.isActive === 'true' ? 'is-active' : '';
      let html;

      if(isSpecial === true) {
        html = `<div class="projects-pattern__item prepared">
                  <a href="${data.url}" class="project project_special">
                    <span class="project__in">
                      <span class="project__img">
                        <img src="${data.img}" alt="" />
                      </span>
                      <span class="project__title project__title_special">${data.title}</span>
                    </span>
                  </a>
                </div>`;
      }else{
        const neededSvg = data.type === 'PV' ? this.svgPV: this.svgW;
        html=`<div class="projects-pattern__item prepared">
                <a href="${data.url}" class="project ${isActive}">
                  <span class="project__in">
                    <span class="project__img">
                        ${neededSvg}
                    </span>
                    <span class="project__text">${data.desc}</span>
                    <span class="project__title">${data.title}</span>
                    <div class="project__link-wrapper">
                       <span class="project__link">Read more</span>
                    </div>
                  </span>
                </a>
             </div>`;
      }

      return html;
    }
    _scrollToStart(scrollSpeed = 1200) {
      HTMLBODY.animate({

        scrollTop: this.list.offset().top - this.scrollOffset
      }, scrollSpeed, 'easeInOutCubic', () => {
      });
    }
    _resize() {
      let self = this;
      this.resizeFunc = throttle(300, () => {
        self._getSizesForScroll();
      });
      WIN.on('resize', this.resizeFunc);
    }
    _getSizesForScroll(bool, scrollSpeed) {
      let additionalOffset = 10;
      if(this.isIE) {
        this.scrollOffset = $('.js-headers .header-sticky.is-sticky').innerHeight() + additionalOffset;
      }else{
        if($(window).width() < 720) {

          this.scrollOffset = (this.filterBox.innerHeight() + $('.js-headers .header-sticky.is-sticky').innerHeight()) + additionalOffset;

        }else{
          // this.scrollOffset = $('.js-headers .header-sticky.is-sticky').innerHeight() + additionalOffset;
          this.scrollOffset = parseInt(this.filterBox.css('top')) ;
        }
      };
      if(bool === true) {
        this._scrollToStart(scrollSpeed);
      }

    }

  }

  DOC.ready(() => {
    new Projects();
  });
})(jQuery);
