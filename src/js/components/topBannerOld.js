import { throttle, debounce } from 'throttle-debounce';
// import normalizeWheel from 'normalize-wheel';
import  './_easing';


(function($) {
  return;
  console.log('helo from topBannerOld.js');
  const WIN = $(window);
  const DOC = $(document);
  const customThrottle = function(callback) {
    let active = false; // a simple flag
    let evt; // to keep track of the last event
    let handler = function() { // fired only when screen has refreshed
      active = false; // release our flag
      callback(evt);
    };
    return function handleEvent(e) { // the actual event handler
      evt = e; // save our event at each call
      if (!active) { // only if we weren't already doing it
        active = true; // raise the flag
        requestAnimationFrame(handler); // wait for next screen refresh
      };
    };
  };
  class StickyNavCss {
    constructor() {
      this.el = $('.js-sticky-nav');
      this.classes = {
        'scrollBlocks': '.js-sticky-scroll-block',
        'navLinks': '.js-sticky-nav-link',
        'navLists': '.js-sticky-nav-list'
      };
      this._start();

    }

    _start() {
      if(!this.el[0]) return;
      this.navList = $(this.classes.navLists);

      this._generateNavHtml();

    }
    _init() {
      this.isIE = $('body').hasClass('ie') ? true: false;
      this.pseudo = $('.js-sticky-nav-pseudo');
      //  80 distance between nav and filter
      this.separateDistance = 80;
      this.topValue = +this.el.data('top');
      this.main = $('.js-main');
      this.mainHeight = this.main.innerHeight();
      this._updateValues();


      if(WIN.width() >= 1024) {
        this.el.removeClass('is-disabled');
      }

      this.links = $(this.classes.navLinks);


      this.isFixed = false;
      this.behindFilter = false;
      this.projectFilter = $('.js-filter-box');
      this.isProjectsBehaviour = this.projectFilter[0] ? true: false;
      this.filterSpecOffset = this.separateDistance + this.topValue + this.pseudoHeight;

      if(this.isProjectsBehaviour === true) {
        this.projectFilter.css({'top': `${this.filterSpecOffset}px`});
        this.projectList = $('.js-projects-list-wrapper');
        this.break = false;
        let interval = setInterval(() => {
          if(window.projectsLoaded === true) {
            if(this.break !== false) return;
            this.break = true;
            console.log('projects loaded');
            clearInterval(interval);
            // this._getNavPosState();
            // this._scroll();
            this._resize();
            if (this.isIE === false) {
              this._moreProjectsClick();
            }
          }
        },100);
      }else{
        // this._getNavPosState();
        // this._scroll();
        this._resize();
      }
      // this.stickyBottomOffset = this.mainHeight - this.projectList.offset().top + this.projectList.innerHeight() - this.projectFilter.innerHeight() - 80;
      this.stickyBottomOffset = this.mainHeight - this.projectList.offset().top;

      this.main.find('div').eq(0).css('margin-top', `${-this.pseudoHeight }px`);
      this.main.prepend(this.el);

      this.el.css({'top': `${this.topValue }px`});
      // this.el.css({'top': `${this.pseudo.offset().top }px`, 'padding-bottom': `${this.stickyBottomOffset}px`});

      this._onClick();
      this._scroll();
    }

    _scroll() {
      this.throttleFunc = throttle(50, this._scrollFunc.bind(this));
      WIN.on('scroll', this.throttleFunc);
      // window.addEventListener('scroll', customThrottle(this._scrollFunc.bind(this)));
      // window.addEventListener('scroll', this._scrollFunc.bind(this));


      // document.addEventListener('mousewheel', function(event) {
      //
      //   const normalized = normalizeWheel(event);
      //   console.log(normalized);
      //   //
      //   // console.log(normalized.pixelX, normalized.pixelY);
      // });
      //

    }
    _scrollFunc(e) {
      if(WIN.width() < 1024) return;
      this._getNavPosState();
      let scrollPos = WIN.scrollTop();
      $(this.classes.scrollBlocks).each((i, el) => {
        let $this  = $(el);
        let headerHeight = $('.js-headers').find('.header-sticky').innerHeight();


        if ($this.offset().top - headerHeight <= scrollPos && $this.offset().top + $this.innerHeight() > scrollPos + headerHeight) {
          this.el.find('a').removeClass('is-active');
          let data = $this.data('id');
          // console.log($(`${this.classes.navLinks}[href="${data}"]`));
          this.el.find(`${this.classes.navLinks}[href="${data}"]`).addClass('is-active');
          return false;
        }else{
          this.el.find('a').removeClass('is-active');
        }
      });

    }
    _updateValues(callNavStateFunc) {
      // this.stickyBottomOffset =
      this.offsetTop = this.pseudo.offset().top - this.topValue;
      console.log();
      this.offsetLeft = this.pseudo.offset().left;
      this.pseudoHeight = this.pseudo.innerHeight();
      this.headersHeight = $('.js-headers').innerHeight();
      this.filterSpecOffset = this.separateDistance + this.topValue + this.pseudoHeight;
      // this.startTopPos = this.pseudo.offset().top - this.headersHeight;
      if(this.isIE === true) {
        this.startTopPos = this.pseudo.offset().top;
      }else{
        this.startTopPos = this.pseudo.offset().top + this.headersHeight;
      }
      if(callNavStateFunc === true) {
        this._getNavPosState();
      }
    }
    _getNavPosState() {
      if(WIN.width() < 1024) {
        this.el.addClass('is-disabled');
        return;
      }else{
        this.el.removeClass('is-disabled');
      };
      // this.el.css({'left': `${this.offsetLeft}px`});
      if(WIN.scrollTop() >= this.offsetTop) {
        if(this.isFixed === false) {
          this.el.addClass('is-visible');
          this.pseudo.addClass('is-hidden');
          this.isFixed = true;
          // this.el.css({'top': `${this.topValue}px`, 'left': `${this.offsetLeft}px`, 'position': 'fixed'});
          // this.el.css({'top': `${this.topValue}px`, 'position': 'fixed'});
        }
        return;
        if(this.isProjectsBehaviour === true) {
          let projectListOffsetTop = this.projectList.offset().top;
          let filterHeight = this.projectFilter.innerHeight();
          let filterOffset;

          if(this.isIE === true) {
            filterOffset = projectListOffsetTop - WIN.scrollTop() - this.filterSpecOffset;
          }else{
            filterOffset = projectListOffsetTop + this.projectList.innerHeight() - WIN.scrollTop() - filterHeight - this.filterSpecOffset;
          }

          if(filterOffset <= 0 ) {
            if(this.behindFilter === false) {
              let newPos;
              if(this.isIE === true) {
                newPos = Math.round(filterOffset + WIN.scrollTop() + this.topValue);
              }else{
                newPos = Math.round(filterOffset + WIN.scrollTop() + this.topValue - this.headersHeight);
              }
              // this.el.addClass('fixed').css({'top': `${newPos}px`, 'left': `${this.offsetLeft}px`, 'position': 'absolute'});
              this.el.addClass('fixed').css({'top': `${newPos}px`, 'position': 'absolute'});
              this.behindFilter = true;
            }
          }else{
            if(this.behindFilter === true) {
              // this.el.addClass('fixed').css({'top': `${this.topValue}px`, 'left': `${this.offsetLeft}px`, 'position': 'fixed'});
              this.el.addClass('fixed').css({'top': `${this.topValue}px`, 'position': 'fixed'});
              this.behindFilter = false;
            }
          }

        }
        //
      }else{
        // if(this.isFixed === true) {

        this.isFixed = false;
        this.el.removeClass('is-visible');
        this.pseudo.removeClass('is-hidden');
        // this.el.css({'top': `${this.startTopPos}px`, 'left': `${this.offsetLeft}px`, 'position': 'absolute'});
        // this.el.css({'top': `${this.startTopPos}px`, 'left': `${this.offsetLeft}px`, 'position': 'absolute'});

        // }

      }

    }



    _onClick() {
      // let self = this;
      this.links.on('click', (e) => {
        e.preventDefault();
        let $this = $(e.target);
        // if($this.hasClass('is-active')) return;
        let target =  $(`${this.classes.scrollBlocks}[data-id="${$this.attr('href')}"]`);
        this.el.find('a').removeClass('is-active');

        let headerHeight = $('.js-headers').find('.header-sticky').innerHeight();

        $('html, body').stop().animate({
          'scrollTop': target.offset().top - headerHeight + 1
        }, 1200, 'easeInOutCubic', function() {
          // WIN.on('scroll', self.throttleFunc);
        });
      });
    }

    _resize() {
      let self = this;
      this.resizeFunc = throttle(300, () => {
        // this.resizeFunc = debounce(350, () => {
        // if(WIN.width() >= 1024) {
        self._updateValues(true);
        // }
      });
      WIN.on('resize', this.resizeFunc);
    }


    _generateNavHtml() {
      let dataArr = [];
      $(this.classes.scrollBlocks).each((index, el) => {
        let $this = $(el);
        let obj = {'id': $this.data('id'), 'name': $this.data('name')};
        dataArr.push(obj);
      });

      this.navList.each((index, el) => {
        for(let i = 0; i < dataArr.length; i++) {
          let linkHtml = `<a href="${dataArr[i].id}" class="sticky-nav__item js-sticky-nav-link">${dataArr[i].name}</a>`;
          $(el).append(linkHtml);
        }
      });


      this._init();
    }
    _moreProjectsClick() {
      $('.js-more-btn').on('click', (e) => {
        e.preventDefault();
        let interval = setInterval(() => {
          if(window.projectsMoreClicked === true) {
            window.projectsMoreClicked = false;
            clearInterval(interval);
            // this._getNavPosState();
            this.el.css({ 'bottom': `${$('.js-projects-list-wrapper').offset().top}px`});
            console.log('more click from topBanner.js');
          }
        },50);
      });
    }



  }
  DOC.ready(() => {

    DOC.StickyNavCss = new StickyNavCss();

  });
})(jQuery);
