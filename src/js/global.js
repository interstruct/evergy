export const WIN = $(window);
export const DOC = $(document);
export const BODY = $('body');
export const HTMLBODY = $('html, body');

export const HEADER = $('.header');
export const HEADERSTICKY = $('.header-sticky');
export const MAIN = $('.main');
export const FOOTER = $('.footer');

export const BREAKPOINTS = {
  desktopLG: window.matchMedia('(max-width: 1499px)'),
  desktopMD: window.matchMedia('(max-width: 1279px)'),
  desktopSM: window.matchMedia('(max-width: 1023px)'),
  tabletLG: window.matchMedia('(max-width: 991px)')
};

export const PREVENTDEFAULT = e => {
  e.preventDefault();
  console.log('test');
};
